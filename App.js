/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
// import type {Node} from 'react';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';
import Login from './components/auth/login/login'
import Register from './components/auth/register/register'
import Main from './components/main';
import { LogBox, Text, View } from 'react-native';
import save from './components/main/saveScreen/save';
import comment from './components/main/commentScreen/comment';
// import SearchTopTab from './components/main/searchScreen/SearchTopTab/searchTopTab'
import Message from './components/main/messageScreen/messageDetail/message';
import ListMessage from './components/main/messageScreen/listMessage/listMessage';
import { Provider, useDispatch, useSelector } from 'react-redux'
import { store } from './redux/store'
import profile from './components/main/profileScreen/profile';
import ListAccounts from './components/main/searchScreen/listAccount/listAccount';
import editProfileScreen from './components/main/editProfileScreen/editProfileScreen';
import listLikes from './components/main/feedScreen/ListLikes/listLikes';
import editNameScreen from './components/main/editProfileScreen/editContent/editNameScreen';
// import listImage from './components/main/saveScreen/listFiles/listImage';
import listMultiImage from './components/main/profileScreen/topTab/images/listMultiImage/listMultiImage';
import PushNotification from "react-native-push-notification";
import test from './components/main/messageScreen/messageDetail/test';
import OpenVideo from './components/main/profileScreen/topTab/videos/openVideo';

// import PushNotificationIOS from "@react-native-community/push-notification-ios";



// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCXLmeQDDhgnD7Mc9unoOCXfGFi6CbYwsw",
  authDomain: "instagram-app-f3d8d.firebaseapp.com",
  databaseURL: "https://instagram-app-f3d8d-default-rtdb.firebaseio.com",
  projectId: "instagram-app-f3d8d",
  storageBucket: "instagram-app-f3d8d.appspot.com",
  messagingSenderId: "667814295313",
  appId: "1:667814295313:web:05870d02051472646efeb0",
  measurementId: "G-V4VQKMFJ28"
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}



// Must be outside of any component LifeCycle (such as `componentDidMount`).
// PushNotification.configure({
//   // (optional) Called when Token is generated (iOS and Android)
//   onRegister: function (token) {
//     console.log("TOKEN:", token);
//   },

//   // (required) Called when a remote is received or opened, or local notification is opened
//   onNotification: function (notification) {
//     console.log("NOTIFICATION:", notification);
//   },

//   // IOS ONLY (optional): default: all - Permissions to register.
//   permissions: {
//     alert: true,
//     badge: true,
//     sound: true,
//   },

//   // Should the initial notification be popped automatically
//   // default: true
//   popInitialNotification: true,

//   /**
//    * (optional) default: true
//    * - Specified if permissions (ios) and token (android and ios) will requested or not,
//    * - if not, you must call PushNotificationsHandler.requestPermissions() later
//    * - if you are not using remote notification or do not have Firebase installed, use this:
//    *     requestPermissions: Platform.OS === 'ios'
//    */
//   requestPermissions: true,
// });


GoogleSignin.configure({
  scopes: ['https://www.googleapis.com/auth/drive.readonly'],
  webClientId: "667814295313-6sj79simi1saatibh0trr4fmppjo1r5d.apps.googleusercontent.com",
  offlineAccess: true,
});




// const Section = ({children, title}): Node => {
const Stack = createStackNavigator();
const App = ({ navigation, route }) => {
  LogBox.ignoreLogs(['Remote debugger']);
  const [loginIn, setLogin] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const theReceiver = useSelector(state => state.messState.receiver);

  auth().onAuthStateChanged(user => {
    if (!user) {
      setLogin(false);
      setLoaded(true)
    }
    else {
      setLogin(true);
      setLoaded(true)
    }
  })

  if (loaded) {
    <View>
      <Text>Loading</Text>
    </View>
  }
  if (!loginIn) {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Login' >
          <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
          <Stack.Screen name='Register' component={Register} options={{ headerTitle: '' }} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
  else {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Login'>
          <Stack.Screen name='Main' component={Main} options={{ headerShown: false }} />
          {/* <Stack.Screen name='Save' component={save} navigation={navigation} options={{ headerTitle: 'New Post' }} /> */}
          <Stack.Screen name='Comment' component={comment} navigation={navigation} options={{ headerTitle: 'Comments' }} />
          <Stack.Screen name='Message' component={Message} navigation={navigation} options={({ route }) => ({ headerTitle: route.params.receiver })} />
          <Stack.Screen name='ListMessage' component={ListMessage} navigation={navigation} options={{ headerTitle: auth().currentUser.displayName }} />
          <Stack.Screen name='Profile' component={profile} navigation={navigation} />
          <Stack.Screen name='ListAccounts' component={ListAccounts} navigation={navigation} options={{ headerTitle: '' }} />
          <Stack.Screen name='test' component={test} navigation={navigation} options={{ headerTitle: '' }} />
          <Stack.Screen
            name='EditProfile'
            component={editProfileScreen}
            options={{
              headerTitle: 'Edit Profile',
              // headerRight: () => (
              //   <TouchableOpacity onPress={() => { dispatch(fetchUser()); navigation.popToTop() }} style={{ paddingHorizontal: 10, marginRight: 10 }}>
              //     <Ionicons name="checkmark" size={28} color="#50C6FF" />
              //   </TouchableOpacity>
              // ),
            }}
            navigation={navigation}
          />
          <Stack.Screen name='ListLikes' component={listLikes} navigation={navigation} options={{
            headerShown: false
          }} />
          <Stack.Screen name='EditName' component={editNameScreen} />
          {/* <Stack.Screen name='ListImage' component={listImage} /> */}
          <Stack.Screen name='ListMultiImage' component={listMultiImage} navigation={navigation} options={{ headerShown: false }} />
          <Stack.Screen name='OpenVideo' component={OpenVideo} navigation={navigation} options={{ headerTitle: 'Video' }} />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }

};

export default App;

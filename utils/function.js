import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
export const findUserById = (uid) => {
    return firestore()
        .collection('users')
        .doc(uid)
        .get()
        .then(doc => {
            if (doc.exists) {
                return {
                    id: doc.id,
                    ...doc.data()
                }
            }
        });
}
export const readMess = (roomId) => {
    firestore()
        .collection('messages')
        .doc(roomId)
        .collection('contents')
        .where('hasRead', '==', false)
        .get()
        .then(snapshot => {
            snapshot.docs.map(doc => {
                firestore()
                    .collection('messages')
                    .doc(roomId)
                    .collection('contents')
                    .doc(doc.id)
                    .update({ hasRead: true })
            })
        });
}
export const getLastMess = (roomId) => {
    return firestore()
        .collection('messages')
        .doc(roomId)
        .collection('contents')
        .orderBy('creation', 'desc')
        .limit(1)
        .get()
        .then(snapshot => {
            const data = snapshot.docs.map(doc => {
                return { id: doc.id, ...doc.data() }
            })
            return data[0];
        })
}


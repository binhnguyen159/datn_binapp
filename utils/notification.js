import axios from 'axios'
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';

export const sendNotification = async (token, title, body) => {
    const data = {
        message: {
            token: token,
            notification: {
                title: title,
                body: body,
            },
        }
    };
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ya29.a0AfH6SMAJpBw8sxpfijgMCxoeuMvRqS3iPoXbjgcyPV-VDZtQQKg6nq_E5TzU-R6Z_evEhnrBSVPhmPK8ViO8QO2pbJIB0KlAVUn948XUURgDqQ5hYI4bABRfYD4hDW-EyJ3zHURQsycJnJlRYzBrfszpmRhn'
    }

    await axios.post('https://fcm.googleapis.com/v1/projects/instagram-app-f3d8d/messages:send',
        data, { headers: headers }
    )
        .then(function (response) {
            return response
        })
        .catch(function (error) {
            return error
        });
}

export const saveTokenDevice = async () => {
    return messaging()
        .getToken()
        .then(token => {
            return token;
        });
}

export const sendNotificationToMultiDevice = async (tokens, name, content) => {
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'key = AAAAm3zVdxE:APA91bEQPM8mzXuaxFGvNCq-Agau3wEajLZTEsfIWfldsiSItqtNCamHhbCwh26GVDB89A3YM70fo7C01bCLB5KTCnE1SW5D9VR03pSdypR09bn3KK4SEzwOBR6TwLV_l6NzWyBWWj8K'
    };
    const data = {
        "registration_ids": tokens,
        "notification": {
            "title": name, "body": content,
            "sound": "default",
            "click_action": "FCM_PLUGIN_ACTIVITY",
            "icon": "fcm_push_icon"
        },
        "data": {
            "message": content
        }
    }
    await axios.post('https://fcm.googleapis.com/fcm/send',
        data, { headers }
    )
        .then(function (response) {
            return response
        })
        .catch(function (error) {
            return error
        });
}
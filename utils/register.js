import * as yup from 'yup'


export const registerSchema = yup.object({
    name: yup.string().required("Name is require"),
    email: yup.string().required("Email is require"),
    password: yup.string().min(6, "Password must have at least 6 character").required("Password is require"),
    confirmPassword: yup.string().min(6, "Confirm password must have at least 6 character").required("Confirm password is require"),
})

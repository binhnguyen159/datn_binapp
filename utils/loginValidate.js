import * as yup from 'yup'

export const loginSchema = yup.object({
    email: yup.string().required("Email is required").email("Email must be an email"),
    password: yup.string().min(6, "Password must have at least 6 character").required("Password is required")
})
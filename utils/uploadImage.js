import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';


export const uploadImage = async (uri) => {
    try {
        const childPath = `post/${auth().currentUser.uid}/${Math.random().toString(36)}`;
        // console.log(childPath);

        const response = await fetch(uri);
        const blob = await response.blob();

        const task = storage().ref().child(childPath).put(blob);

        const taskProgress = (snapshot) => {
            console.log(`transferred: ${snapshot.bytesTransferred}`);
        }
        const taskCompleted = () => {
            task.snapshot.ref.getDownloadURL().then((snapshot) => {
                firestore()
                    .collection('users')
                    .doc(auth().currentUser.uid)
                    .update({
                        avatar: snapshot,
                    })
                auth().currentUser.updateProfile({
                    photoURL: snapshot,
                }).then(function () {
                    console.log('success');
                }).catch(function (error) {
                    console.log('err: ', error);
                });
                console.log('task completed: ');
            })
        }
        const taskError = snapshot => {
            console.log('wwwwwwwwwwww', snapshot)
        }


        task.on("state_changed", taskProgress, taskError, taskCompleted);
    }
    catch (error) {
        console.log('?????????????????', error);
    }
}
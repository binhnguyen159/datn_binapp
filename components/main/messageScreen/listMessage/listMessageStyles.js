import { Dimensions, StyleSheet } from "react-native";

export const listMessageStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    listMess: {
        flex: 1,
    }
})
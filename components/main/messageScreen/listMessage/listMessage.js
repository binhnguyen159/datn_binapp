import React, { useEffect, useState } from 'react'
import { FlatList, ScrollView, Text, TextInput, View } from 'react-native'
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { listMessageStyles } from './listMessageStyles';
import { Avatar } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { clearDataMess, fetchListContentMessage, fetchMessages } from '../../../../redux/actions/messages.action';
import { useDispatch, useSelector } from 'react-redux';




export default function ListMessage({ navigation }) {
    const messages = useSelector(state => state.messState.messages);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchMessages());
    }, []);
    messages.sort(function (x, y) {
        return y.lastMess?.creation?._seconds - x.lastMess?.creation?._seconds;
    })
    return (
        <View style={listMessageStyles.container}>
            <View style={listMessageStyles.listMess}>
                <FlatList
                    data={messages}
                    keyExtractor={item => item.roomId}
                    renderItem={({ item }) => {
                        let date;
                        if (item?.lastMess?.creation?._seconds)
                            date = new Date(item?.lastMess?.creation?._seconds * 1000);
                        return (
                            <TouchableOpacity onPress={() => {
                                dispatch(fetchListContentMessage(item.roomId, 0));
                                navigation.navigate('Message', { info: item, receiver: item.user.name });
                            }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 10, padding: 10, borderBottomWidth: 0.5, }}>
                                    <View style={{ marginRight: 20 }}>
                                        <Avatar.Image source={{ uri: item?.user?.avatar }} />
                                    </View>
                                    <View style={{ flexDirection: 'column', justifyContent: 'center', width: '75%' }}>
                                        <Text>{item?.user?.name}</Text>
                                        {
                                            item?.lastMess?.hasRead === false &&
                                                item?.lastMess.owner !== auth().currentUser.uid ?
                                                (
                                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                                        <Text numberOfLines={1} style={{ fontWeight: 'bold', width: '65%' }}>{item?.lastMess?.content}</Text>
                                                        {!date ? <View /> : <Text style={{ fontWeight: 'bold', marginLeft: 20 }}>  -  {`${date.getHours()}:${date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()}`}</Text>}
                                                    </View>
                                                )
                                                :
                                                (
                                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                                        <Text numberOfLines={1} style={{ maxWidth: '65%' }}>{item?.lastMess?.content}</Text>
                                                        {!date ? <View /> : <Text style={{ marginLeft: 20 }}>  -  {`${date.getHours()}:${date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()}`}</Text>}
                                                    </View>
                                                )
                                        }
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )

                    }
                    }
                />

            </View>
        </View>
    )
}

import React, { useCallback, useEffect, useState } from 'react'
import { View } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { useDispatch, useSelector } from 'react-redux';
import { fetchListContentMessage, fetchReceiver } from '../../../../redux/actions/messages.action';
import { sendNotificationToMultiDevice } from './../../../../utils/notification';
import { readMess } from '../../../../utils/function';
import { GiftedChat, Bubble, Send } from 'react-native-gifted-chat';

export default function Message({ route }) {
    const listContent = useSelector(state => state.messState.listMessageOfOne);
    const dispatch = useDispatch();
    const [receiver, setReceiver] = useState({})

    useEffect(async () => {
        firestore()
            .collection('messages')
            .doc(route.params.info.roomId)
            .update({ update: Math.floor(Math.random() * 100) })
        if (route.params?.info?.lastMess?.owner !== auth().currentUser.uid) {
            readMess(route.params.info.roomId)
        }

        const receivers = await firestore()
            .collection('users')
            .doc(route.params.info.uid)
            .get();
        dispatch(fetchReceiver(route.params.info.uid));
        setReceiver(receivers.data());
        dispatch(fetchListContentMessage(route.params.info.roomId, 0));
    }, [])

    const onSend = async (messages = []) => {
        await firestore()
            .collection('messages')
            .doc(route.params.info.roomId)
            .collection('contents')
            .add({
                content: messages[0].text,
                creation: firestore.FieldValue.serverTimestamp(),
                owner: messages[0].user._id,
                hasRead: false
            });
        await firestore()
            .collection('messages')
            .doc(route.params.info.roomId)
            .update({ update: Math.floor(Math.random() * 100) })
        sendNotificationToMultiDevice(receiver.tokens, route.params.receiver, messages[0].text)
    };
    let listMessage = listContent?.map(message => ({
        _id: message.id,
        text: message.content,
        createdAt: message?.creation?._seconds * 1000,
        user: {
            _id: message.owner,
            name: route.params.info.user.name,
            avatar: route.params.info.user.avatar,
        }
    }));



    // setMessages(listMessage);
    const renderBubble = (props) => {
        return (
            <Bubble
                {...props}
                wrapperStyle={{
                    right: {
                        backgroundColor: '#2e64e5',
                        marginVertical: 5,
                    },
                    left: {
                        backgroundColor: 'white',
                        marginVertical: 5,
                    }
                }}
                textStyle={{
                    right: {
                        color: '#fff'
                    },
                }}
            />
        )
    }
    const renderSend = (props) => {
        return (
            <Send {...props}>
                <View>
                    <MaterialCommunityIcons name="send-circle" style={{ marginBottom: 5, marginRight: 15 }} size={32} color="#2e64e5" />
                </View>
            </Send>
        )
    }
    const scrollToBottomComponent = (props) => {
        return (
            <FontAwesome name="angle-double-down" size={22} color="#333" />
        )
    }

    return (
        // <View >

        <GiftedChat
            messages={listMessage}
            onSend={messages => onSend(messages)}
            user={{
                _id: auth().currentUser.uid,
            }}
            renderBubble={renderBubble}
            alwaysShowSend
            renderSend={renderSend}
            scrollToBottom
            scrollToBottomComponent={scrollToBottomComponent}
        />
        // </View>
    )
}
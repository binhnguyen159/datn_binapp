import React, { useState } from 'react'
import { View, Text, TouchableOpacity, TouchableWithoutFeedback, Keyboard, TouchableHighlight } from 'react-native'
import { } from 'react-native-gesture-handler';

export default function detailMessage({ date, content, color }) {
    const [isShowTime, setIsShowTime] = useState(false);
    const [isShowOption, setIsShowOption] = useState(false);
    const onPress = () => {
        setIsShowTime(!isShowTime);
        setIsShowOption(false);
        console.log('press ', isShowOption);
    }
    const onLongPress = () => {
        setIsShowOption(true);
        console.log('long press ', isShowOption);
    }
    return (
        <View style={{
            maxWidth: '70%',
        }}>

            <TouchableOpacity
                delayLongPress={700}
                onPress={() => onPress()}
                onLongPress={() => onLongPress()}
            >
                <Text style={{
                    textAlign: 'center',
                    fontSize: 16,
                    fontWeight: '100',
                    marginRight: 10,
                    borderWidth: color === 'white' ? 1 : 0,
                    borderRadius: 15,
                    paddingHorizontal: 30,
                    paddingVertical: 4,
                    backgroundColor: color,
                }}>
                    {content}
                </Text>
            </TouchableOpacity>
            {isShowTime ? (
                (<Text style={{
                    fontSize: 12,
                    // alignItems: 'center'
                    textAlign: 'center'
                }}>
                    {/* {`${date}`} */}
                    {`${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} - ${date.getHours()}:${date.getMinutes()}`}
                </Text>)
            ) : null
            }
        </View>
    )
}

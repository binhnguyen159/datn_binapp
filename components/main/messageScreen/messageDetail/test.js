import React, { useEffect } from 'react'
import { View, Text, Button } from 'react-native'
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import axios from 'axios'

export default function test() {

    useEffect(() => {



        messaging()
            .getToken()
            .then(token => {
                setToken(token)
                return saveTokenToDatabase(token);
            });

        return messaging().onTokenRefresh(token => {
            saveTokenToDatabase(token);
        });
    }, [])

    const sendMessage = async () => {
        console.log('in');
        const me = await firestore()
            .collection('users')
            .doc('OO4UITgB5ph71Uj5VHJDplGQYlj2')
            .get();
        const you = await firestore()
            .collection('users')
            .doc('FjeYtVIRdmXzQmuaJXt1m99MB5Q2')
            .get();
        const data = {
            message: {
                token: "fn65ds9BS7SMXT43j9qi0d:APA91bGm8amxMzHlmmDhW2HVSMWBngyHHlrbiG2o5BBR_764NajMLyFWHPnMaAwXZspqI0RCvm0WTV2SwNqdaCWqigwDkYTeQu-D6ky4kQYHB4_3VjNzcglkt9dSeGn9ToCcVk6eB85Y",
                notification: {
                    title: "binh",
                    body: "ok duoc roi ne!"
                },
            }
        };
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ya29.a0AfH6SMCBR93aFJEpBRhV74LC5W9s6-B2HvdTLxzhSHATYmoRv3nMyfzjKyJAcPHvgkBf7pJYnS0wL4RXXh6x1yHniVvv95KqJoIaeou11h__mwerPmHtZAjmJ2iWnrqoBaKIBNkZTAqYd8QXqDtVlU7dCTtB'
        }

        await axios.post('https://fcm.googleapis.com/v1/projects/instagram-app-f3d8d/messages:send',
            data, { headers: headers }
        )
            .then(function (response) {
                return response
            })
            .catch(function (error) {
                return error
            });
    }
    return (
        <View>
            <Button title='send' onPress={() => { sendMessage() }} />
        </View>
    )
}

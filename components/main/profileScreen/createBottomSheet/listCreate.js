import React, { useEffect } from 'react'
import { Text, TouchableOpacity, View } from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import BottomSheet from 'reanimated-bottom-sheet';

export default function listCreate() {
    useEffect(() => {
        sheetRef.current.snapTo(0)
    })
    const renderContent = () => (
        <View
            style={{
                backgroundColor: 'white',
                padding: 16,
                height: 450,
            }}
        >
            <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 18 }}>Create New</Text>

            <View style={{ flex: 1, }}>
                <TouchableOpacity onPress={() => navigation.navigate('Add')}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }} >

                        <FontAwesome5Icon style={{ marginRight: 20 }} name='image' size={24} color={'black'} />
                        <Text style={{ borderBottomWidth: 1 }}>Post Image</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }} >
                        <FontAwesome5 name='image' size={24} color={'black'} />
                        <Text style={{ borderBottomWidth: 1 }}>Post Video</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }} >
                        <FontAwesome5 name='image' size={24} color={'black'} />
                        <Text style={{ borderBottomWidth: 1 }}>Post Status</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
    const sheetRef = React.useRef(null);

    return (
        <BottomSheet
            ref={sheetRef}
            snapPoints={[450, 300, 0]}
            borderRadius={10}
            renderContent={renderContent}
        />

    )
}

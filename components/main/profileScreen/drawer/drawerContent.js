
import React, { useEffect } from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'
import setting from '../settingsScreen/setting';
import profile from '../profile';
import auth from '@react-native-firebase/auth';

const Drawer = createDrawerNavigator();
export default function drawerContent({ navigation, route }) {
    return (
        <Drawer.Navigator drawerPosition='right'>
            <Drawer.Screen name='Profile' component={profile} />
            <Drawer.Screen name='About' component={setting} />
        </Drawer.Navigator>
    )
}
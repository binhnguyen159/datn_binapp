import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore';
import messaging from '@react-native-firebase/messaging';
import FlatButton from './../../../../shared/button';
import FlatButton2nd from './../../../../shared/buttonFill';

export default function setting() {

    const signOut = async () => {
        try {
            await messaging().getToken()
                .then(async deviceToken => {
                    const user = await firestore()
                        .collection('users')
                        .doc(auth().currentUser.uid)
                        .get();
                    console.log(deviceToken);
                    const newTokens = await user.data().tokens.filter(token => token !== deviceToken)
                    firestore()
                        .collection('users')
                        .doc(auth().currentUser.uid)
                        .update({
                            tokens: newTokens
                        })
                });
            auth().signOut();
        } catch (error) {
            console.log('sign out: ', error);
        }
    }

    return (
        <View style={styles.container}>
            <View style={{ flex: 1, marginTop: 20 }}>
                <Text style={{ textAlign: 'center' }}>BIN APP - OUR SOCIAL MEDIA</Text>
            </View>
            <View style={{ flex: 8, justifyContent: 'center', marginLeft: 10 }}>
                <Text style={styles.text}>Author: Nguyễn Thanh Bình</Text>
                <Text style={styles.text}>University: UTC-HCMC</Text>
                <Text style={styles.text}>Major: Information Technology</Text>
                <Text style={styles.text}>Class: K58</Text>
            </View>
            <View style={{ flex: 1, marginBottom: 0 }}>
                <FlatButton2nd
                    title='Log out'
                    onPress={() => signOut()}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    text: {
        marginVertical: 5,
        fontSize: 16
    }
})
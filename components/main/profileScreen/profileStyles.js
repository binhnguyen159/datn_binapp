import { StyleSheet } from "react-native";
import Animated from 'react-native-reanimated';

export const profileStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
    },
    headerInfo: {
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center'

    },
    headerFeature: {
        flexDirection: 'row',
    },
    content: {
        flexDirection: "row",
        justifyContent: 'space-around',
        alignItems: 'center',
        marginVertical: 20,
    },
    button: {
        color: 'black',
    },
    bottomSheet: {
        backgroundColor: 'white',
        padding: 16,
        height: 450,

    },
    containerTitleCreateBottomSheet: {
        backgroundColor: '#FFFFFF',
        shadowColor: '#333333',
        shadowOffset: { width: -1, height: -3 },
        shadowRadius: 2,
        shadowOpacity: 0.4,
        // elevation: 5,
        paddingTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,

    },
    titleCreateBottomSheet: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 18,
        borderBottomWidth: 1,
        paddingBottom: 10,
        color: 'black',
    },
    elementCreateBottomSheet: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        borderBottomWidth: 1,
    },
    icon: {
        marginRight: 20
    },
    contentBottomSheet: {
        width: '100%'
    },
    closeBottomSheet: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
    }
})
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Setting from '../settingsScreen/setting';
const Stack = createStackNavigator();
export default function settingStackScreen() {
    return (
        <Stack.Navigator >
            <Stack.Screen name='Settings' component={Setting} />
        </Stack.Navigator>
    )
}

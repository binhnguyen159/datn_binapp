import React, { useEffect, useState } from 'react'
import { View, Text, FlatList, Image, Dimensions } from 'react-native'
import { findUserById } from './../../../../../../utils/function';
import Header from './../../../../../../shared/header';
import { useDispatch } from 'react-redux';
import firestore from '@react-native-firebase/firestore';

export default function listMultiImage({ route, navigation }) {
    const [user, setUser] = useState({})
    const dispatch = useDispatch();
    useEffect(async () => {
        const userLoad = await findUserById(route.params.item.uid);
        setUser(userLoad)
    }, [route.params.item])

    const loadOne = (data) => {
        return (
            <Image
                style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').width, marginTop: 40 }}
                source={{ uri: data }}
            />
        )
    }

    const loadMulti = (data) => {
        return (
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={data}
                renderItem={({ item }) => (
                    <View style={{ marginTop: 20 }}>

                        <Image
                            style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').width, marginTop: 40 }}
                            source={{ uri: item }}
                        />
                    </View>
                )}
            />
        )
    }


    if (!route.params.one) {
        return (
            <View style={{ height: '100%' }}>
                <Header title="Image 's Detail" icon='trash' navigation={navigation} onPress={() => {
                    firestore()
                        .collection('posts')
                        .doc(route.params.item.uid)
                        .collection('userPosts')
                        .doc(route.params.item.id)
                        .delete({})
                    navigation.goBack();
                }} />
                {loadMulti(route.params.item.downloadURL)}
            </View>
        )
    }
    if (route.params.one) {
        return (
            <View style={{ flex: 1 }}>
                <Header title="Image 's Detail" icon='trash' navigation={navigation} onPress={() => {
                    firestore()
                        .collection('posts')
                        .doc(route.params.item.uid)
                        .collection('userPosts')
                        .doc(route.params.item.id)
                        .delete({})
                    navigation.goBack();
                }} />
                {loadOne(route.params.item.downloadURL)}
            </View>

        )
    }
}

import React, { useEffect, useState } from 'react'
import { FlatList, Image, View } from 'react-native'
import { topTabStyles } from '../topTabStyles'
import { useSelector, useDispatch } from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { TouchableOpacity } from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';


export default function posts({ navigation }) {
    // const data = [
    //     { key: '1', link: 'https://teletype.in/files/bf/82/bf826813-1c07-4adf-bfa8-3e3f670cd3c5.jpeg' },
    //     { key: '2', link: 'https://cattime.com/assets/uploads/2018/05/national-best-friends-day-cats-1.jpg' },
    //     { key: '3', link: 'https://i.pinimg.com/564x/a9/0d/9f/a90d9f99fbc52eb15bfc2782e97e9b20.jpg' },
    //     { key: '4', link: 'https://i.pinimg.com/236x/25/0d/d0/250dd038b6c81d9040b87346cb05d627.jpg' },
    //     { key: '5', link: 'https://i.pinimg.com/236x/9d/d9/17/9dd9174626ecf39e1faf35af7eb4c0f9.jpg' },
    // ];

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.userState.currentUser);
    const imagePosts = useSelector(state => state.userState.imagePosts);
    useEffect(() => {
        // console.log(imagePosts);
        // dispatch(fetchUserPosts());
        // dispatch(fetchImagePostsUser(auth().currentUser.uid))
    }, [])
    const loadImage = (data) => {
        return data?.map((item) => {
            return (
                <Image source={{ uri: item }} style={{ flex: 1, aspectRatio: 1 / 1, }} resizeMode='cover' />
            )
        })
    }

    return (
        <View style={topTabStyles.container}>
            <FlatList
                numColumns={3}
                horizontal={false}
                data={imagePosts}
                renderItem={({ item }) => {
                    if (item.type === 'image') {
                        return (
                            <View style={{ flex: 1 / 3, margin: 1 }}>
                                <TouchableOpacity onPress={() => {
                                    console.log(item.uid);
                                    if (item.uid === auth().currentUser.uid)
                                        navigation.navigate('ListMultiImage', { item, one: true })
                                }}>
                                    <Image
                                        style={{ flex: 1, aspectRatio: 1 / 1, }}
                                        resizeMode='cover'
                                        source={{ uri: item.downloadURL }} />
                                </TouchableOpacity>
                            </View>
                        )
                    }
                    if (item.type === 'mixed') {
                        return (
                            <View style={{ flex: 1 / 3, margin: 1 }}>
                                <TouchableOpacity onPress={() => {
                                    if (item.uid === auth().currentUser.uid)
                                        navigation.navigate('ListMultiImage', { item, one: false })
                                }}>
                                    <Image
                                        style={{ flex: 1, aspectRatio: 1 / 1, }}
                                        resizeMode='cover'
                                        source={{ uri: item.downloadURL[0] }} />
                                    {item.downloadURL.length > 1 ?
                                        <MaterialCommunityIcons style={{ position: 'absolute', top: 0, right: 0 }} name="checkbox-multiple-blank" size={24} color="white" />
                                        : null}
                                    {/* <MaterialCommunityIcons style={{ position: 'absolute', top: 0, right: 0 }} name="checkbox-multiple-blank-outline" size={24} color="gray" /> */}
                                </TouchableOpacity>
                            </View>
                        )
                    }

                    // {
                    //     item.type === 'image' ?
                    //         <Image style={
                    //             { flex: 1, aspectRatio: 1 / 1, }
                    //         } resizeMode='contain' source={{ uri: item.downloadURL }} />

                    //         :
                    //         <View />
                    // }
                    // {
                    //     item.type === 'mixed' ?
                    //         <View style={{ flex: 1 / 3, margin: 1 }}>
                    //             <Pages style={{ flex: 1 }}>
                    //                 {feedDetail(item.downloadURL)}
                    //             </Pages>
                    //         </View>
                    //         :
                    //         null
                    // }
                    // return (
                    //     <View style={{ flex: 1 / 3, margin: 1 }}>
                    //         <Image style={{ flex: 1, aspectRatio: 1 / 1, }}
                    //             source={{ uri: item.downloadURL }}
                    //         />
                    //     </View>
                    // )
                }} />
        </View>
    )
}

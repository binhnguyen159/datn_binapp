import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import PostsScreen from './images/images';
import VideoScreen from './videos/videos';

const Tab = createMaterialTopTabNavigator();
export default function TopTab() {
    return (
        <Tab.Navigator
            tabBarLabel={{
                color: 'black'
            }}
        >
            <Tab.Screen name='Images' component={PostsScreen} />
            <Tab.Screen name='Videos' component={VideoScreen} />
        </Tab.Navigator>
    )
}

import React, { useEffect, useState } from 'react'
import { FlatList, View } from 'react-native'
import Video from 'react-native-video';
import { useDispatch, useSelector } from 'react-redux';
import { fetchVideoPostsUser } from '../../../../../redux/actions/user.actions';
import { topTabStyles } from '../topTabStyles'
import auth from '@react-native-firebase/auth';
import { TouchableOpacity } from 'react-native';

export default function videos({ navigation }) {
    const videoPosts = useSelector(state => state.userState.videoPosts);
    // console.log(videoPosts);
    return (
        <View style={topTabStyles.container}>
            <FlatList
                numColumns={3}
                horizontal={false}
                data={videoPosts}
                renderItem={({ item }) => {
                    return (
                        <View style={{ flex: 1 / 3 }}>
                            <TouchableOpacity onPress={() => {
                                if (item.uid === auth().currentUser.uid) {
                                    console.log('item.id ', item.id);
                                    navigation.navigate('OpenVideo', { video: item.downloadURL, postId: item.id })
                                }
                            }}>
                                <Video
                                    muted={true}
                                    style={{ flex: 1, aspectRatio: 1 / 1 }}
                                    resizeMode='contain'
                                    source={{ uri: item.downloadURL }}
                                />
                            </TouchableOpacity>
                        </View>
                    )
                }}
            />
        </View>
    )
}
import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Video, { FilterType } from 'react-native-video'
import { useRoute } from '@react-navigation/native';
import VideoPlayer from 'react-native-video-controls'
import firestore from '@react-native-firebase/firestore';
import Header from './../../../../../shared/header'


export default function OpenVideo({ route, navigation }) {
    console.log(route.params.postId);
    return (
        <View style={styles.container}>
            <Header title='Watch video fullscreen' icon='trash' navigation={navigation} onPress={() => {
                firestore()
                    .collection('posts')
                    .doc(route.params.postId)
                    .delete({})
                navigation.goBack();
            }} />
            <View style={styles.container}>
                <VideoPlayer
                    source={{ uri: route.params.video }}
                    disableBack
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    video: {
        flex: 1,
        backgroundColor: 'gray'
    }
})
import { StyleSheet } from "react-native";

export const topTabStyles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 2,
        backgroundColor: 'white'
    },
})
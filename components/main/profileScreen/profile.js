// import { DrawerContent } from '@react-navigation/drawer'
import React, { useState, useEffect } from 'react'
import { Button, FlatList, Image, Text, View } from 'react-native'
import { profileStyles } from './profileStyles'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Avatar, Provider as PaperProvider } from 'react-native-paper'
import FlatButton2nd from './../../../shared/buttonFill';
import TopTab from './topTab/topTab'
import BottomSheet from 'reanimated-bottom-sheet'
import Animated from 'react-native-reanimated'
import { clearData, fetchImagePostsUser, fetchUser, fetchUserFollowing, fetchUserFollowing2, fetchUserPosts, fetchVideoPostsUser } from '../../../redux/actions/user.actions'
import { useSelector, useDispatch } from 'react-redux';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { fetchFindPostsUser } from './../../../redux/actions/user.actions';
import { clearDatas } from './../../../redux/actions/users.actions';


export default function profile({ navigation, route }) {
    let currentUser = useSelector(state => state.userState.currentUser);
    const [following, setFollowing] = useState(false);
    const [visible, setIsVisible] = useState(false);
    const [user, setUser] = useState({})
    const userPosts = useSelector(state => state.userState.posts);
    const openCreateBottomSheet = () => {
        sheetRef.current.snapTo(0)
        setIsVisible(false);
    }

    const dispatch = useDispatch();

    const fl = useSelector(state => state.userState.following);

    // let data;
    useEffect(() => {
        // dispatch(clearData())
        // dispatch(clearDatas())
        // dispatch(fetchUser())
        // dispatch(fetchUserFollowing(route.params?.uid?.id || auth().currentUser.uid));
        // dispatch(fetchUserPosts());
        setUser(currentUser)

        if (route.params?.uid.id && route.params?.uid.id !== user.uid) {
            {
                setUser(route.params?.uid);
                dispatch(fetchFindPostsUser(route.params?.uid.id));
                dispatch(fetchImagePostsUser(route.params?.uid.id));
                dispatch(fetchVideoPostsUser(route.params?.uid.id));
            }
        }

        if (fl.indexOf(route.params?.uid.id) > -1) {
            setFollowing(true);
        } else {
            setFollowing(false);
        }

    }, [])


    const closeCreateBottomSheet = () => {
        sheetRef.current.snapTo(1)
        setIsVisible(true);
    }

    const renderHeader = () =>
    (
        <View style={profileStyles.containerTitleCreateBottomSheet}>
            <Text style={profileStyles.titleCreateBottomSheet}>Create New</Text>
        </View>
    )

    const onUnFollow = () => {
        firestore()
            .collection('following')
            .doc(auth().currentUser.uid)
            .collection('userFollowing')
            .doc(route.params.uid.id)
            .delete();
        // data = data.filter(data => data === route.params.uid.id);
        setFollowing(false);
    }

    const onFollow = () => {
        firestore()
            .collection('following')
            .doc(auth().currentUser.uid)
            .collection('userFollowing')
            .doc(route.params.uid.id)
            .set({});
        setFollowing(true);
    }

    const renderContent = () => (
        <View style={profileStyles.bottomSheet}>
            <View style={{ flex: 1, }}>
                <TouchableOpacity onPress={() => {
                    navigation.navigate('Add', { type: 'camera' });
                    closeCreateBottomSheet();
                }}
                >
                    <View style={profileStyles.elementCreateBottomSheet} >
                        <FontAwesome5 style={profileStyles.icon} name='image' color={'black'} size={24} />
                        <Text style={profileStyles.contentBottomSheet}>Post Image</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    navigation.navigate('Add', { type: 'video' });
                    closeCreateBottomSheet();
                }}>
                    <View style={profileStyles.elementCreateBottomSheet} >
                        <FontAwesome5 style={profileStyles.icon} name='youtube' color={'black'} size={24} />
                        <Text style={profileStyles.contentBottomSheet}>Post Video</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    navigation.navigate('Add');
                    closeCreateBottomSheet();
                }}>
                    <View style={profileStyles.elementCreateBottomSheet} >
                        <FontAwesome5 style={profileStyles.icon} name='align-left' color={'black'} size={24} />
                        <Text style={profileStyles.contentBottomSheet}>Post Status</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={closeCreateBottomSheet}>
                    <View style={profileStyles.closeBottomSheet} >
                        <FontAwesome5 style={profileStyles.icon} name='times' color={'black'} size={24} />
                        <Text>Cancel</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
    const sheetRef = React.useRef(null);
    const fall = new Animated.Value(1);
    if (user === null) { return <View /> }
    return (
        <PaperProvider>
            {/* <Modal visible={true} style={{ flex: 1 }}> */}
            <View style={profileStyles.container}>
                <Animated.View style={{
                    flex: 1,
                    opacity: Animated.add(0.5, Animated.multiply(fall, 1.0)),
                }}>
                    <View style={profileStyles.header}>
                        <View style={profileStyles.headerInfo}>
                            <View style={{ marginLeft: 20 }}>
                                <Avatar.Image size={24} source={{ uri: route.params !== undefined ? route.params?.uid?.avatar : currentUser?.avatar }} />
                            </View>

                            <View style={{ marginLeft: 10 }}>
                                <Text>{user?.email}</Text>
                            </View>
                        </View>
                        <View style={profileStyles.headerFeature}>
                            <View style={{ marginRight: 20 }}>
                                <TouchableOpacity onPress={openCreateBottomSheet}>
                                    <FontAwesome5 name='plus-circle' color='#000' size={24} />
                                </TouchableOpacity>
                            </View>

                            <View style={{ marginRight: 15 }}>
                                <TouchableOpacity onPress={() => navigation.openDrawer()}>
                                    <FontAwesome5 name='bars' color='#000' size={24} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={profileStyles.content}>
                        <View style={{ flexDirection: 'column', }}>
                            <Avatar.Image size={100} source={{ uri: route.params !== undefined ? route.params?.uid?.avatar : currentUser?.avatar }} />

                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <Text>{userPosts.length}</Text>
                            <Text>posts</Text>
                        </View>
                        {
                            route.params ? null : (
                                <View style={{ alignItems: 'center' }}>
                                    <Text>{fl.length - 1}</Text>
                                    <Text>following</Text>
                                </View>
                            )}
                    </View>
                    <View style={{ marginLeft: 20, marginBottom: 20 }}>

                        <Text>{route.params !== undefined ? route.params?.uid?.name : currentUser?.name}</Text>
                    </View>
                    {
                        (route.params?.uid?.id !== auth().currentUser.uid && route.params) ? (
                            <View>
                                {following ? (
                                    <View >
                                        <FlatButton2nd
                                            title={'Following'}
                                            onPress={() => onUnFollow()}
                                        />

                                        <FlatButton2nd
                                            title={'Message'}
                                            onPress={async () => {
                                                var roomId = await firestore()
                                                    .collection('messages')
                                                    .where('members', 'array-contains', auth().currentUser.uid)
                                                    .get()
                                                    // .where('members', 'array-contains', [auth().currentUser.uid, route.params.uid.id])
                                                    // roomId = roomId.where('members', 'array-contains', auth().currentUser.uid)
                                                    // roomId = roomId.where('members', 'array-contains-any', route.params.uid.id)
                                                    // roomId.get()
                                                    .then(snapshot => {
                                                        let data = snapshot.docs.map(doc => {
                                                            const value = doc.data().members.filter(item => item === route.params.uid.id)
                                                            if (value.length === 1) {
                                                                console.log(value);
                                                                const id = doc.id;
                                                                const data = doc.data()
                                                                return { id, ...data }
                                                            }
                                                        })
                                                        return data;
                                                    })
                                                roomId = roomId.filter(item => item !== undefined)
                                                // console.log('roomId ', roomId);
                                                const data = await firestore()
                                                    .collection('users')
                                                    .doc(user.id)
                                                    .get();
                                                if (roomId.length < 1) {
                                                    return firestore()
                                                        .collection('messages')
                                                        .add({
                                                            members: [auth().currentUser.uid, route.params.uid.id],
                                                            update: 0,
                                                        })
                                                        .then((doc) => {
                                                            navigation.navigate('Message', {
                                                                info: {
                                                                    roomId: doc.id,
                                                                    uid: user.id,
                                                                    user: data.data()
                                                                },
                                                                receiver: route.params?.uid?.name
                                                            });
                                                        });
                                                }
                                                navigation.navigate('Message', {
                                                    info: {
                                                        roomId: roomId[0].id,
                                                        uid: user.id,
                                                        user: data.data()
                                                    },
                                                    receiver: route.params?.uid?.name || auth().currentUser.uid
                                                });
                                            }}
                                        />
                                    </View>
                                )
                                    :
                                    (
                                        <FlatButton2nd
                                            title={'Follow'}
                                            onPress={() => onFollow()}
                                        />
                                    )
                                }
                            </View>)
                            :
                            (<FlatButton2nd
                                title={'Edit Profile'}
                                onPress={() => {
                                    navigation.navigate('EditProfile', { user });
                                }}
                            />)
                    }
                    <View style={{ flex: 1 }}>
                        <TopTab />
                    </View>
                </Animated.View>



                <BottomSheet
                    ref={sheetRef}
                    snapPoints={[300, 0]}
                    renderContent={renderContent}
                    renderHeader={renderHeader}
                    initialSnap={1}
                    callbackNode={fall}
                    enabledGestureInteraction={true}
                />

            </View>
            {/* </Modal> */}
        </PaperProvider>
    )
}

import React, { useEffect, useState } from 'react'
import { Dimensions, Keyboard, TextInput, TouchableWithoutFeedback } from 'react-native';
import { View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Avatar, Modal } from 'react-native-paper';
import { editProfileStyles } from './editProfileStyles';
import ImagePicker from 'react-native-image-crop-picker'
import { uploadImage } from '../../../utils/uploadImage';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUser } from '../../../redux/actions/user.actions';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

export default function editProfileScreen({ navigation }) {
    const user = useSelector(state => state.userState.currentUser)
    const [name, setName] = useState(user.name)
    const dispatch = useDispatch()


    const changeImage = async () => {
        await ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            uploadImage(image.path)
        }).catch(err => console.log(err));
        setTimeout(() => {
            dispatch(fetchUser())
        }, 5000)
        navigation.goBack();
    }

    const saveChange = async () => {
        if (name !== user.name) {
            await firestore()
                .collection('users')
                .doc(auth().currentUser.uid)
                .update({
                    name: name
                })
            await auth().currentUser.updateProfile({ displayName: name })
                .then(function () { console.log('success') })
                .catch(function (err) { console.log(err) });
        }
        dispatch(fetchUser())
        navigation.goBack();
    }

    return (
        <View style={editProfileStyles.container}>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>

                <View style={editProfileStyles.content}>
                    <TouchableOpacity onPress={() => changeImage()} style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 20 }} >
                        <Avatar.Image size={150} source={{ uri: user.avatar }} />
                        <Text style={editProfileStyles.avatarText} >Change Profile Photo</Text>
                    </TouchableOpacity>

                    <View>
                        <View style={editProfileStyles.oneRow}>
                            <Text style={editProfileStyles.textTitle}>Name</Text>
                            <TextInput defaultValue={user?.name} underlineColorAndroid='black'
                                onChangeText={(value) => setName(value)}
                            />
                        </View>
                        <View style={editProfileStyles.oneRow}>
                            <Text style={editProfileStyles.textTitle}>Email</Text>
                            <TextInput underlineColorAndroid='black' defaultValue={user.email} editable={false} />

                        </View>
                        <TouchableOpacity onPress={() => { saveChange() }}>
                            <View style={{ borderColor: 'gray', borderWidth: 2, borderRadius: 5, paddingVertical: 7, marginVertical: 25, }}>
                                <Text style={{ textAlign: "center", fontWeight: 'bold', }}>Save</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </View >
    )
}

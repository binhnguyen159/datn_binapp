import React from 'react'
import { View } from 'react-native'
import { AntDesign } from 'react-native-vector-icons/AntDesign';

export default function headerEditProfile() {
    return (
        <View>
            <AntDesign name="check" size={24} color="black" />
        </View>
    )
}

import { StyleSheet } from "react-native";

export const editProfileStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    content: {
        flex: 1,
    },
    avatar: {

    },
    avatarText: {
        fontSize: 20,
        color: 'blue',
        fontWeight: '500'
    },
    textTitle: {
        color: 'gray',
    },
    oneRow: {
        marginHorizontal: 15,
    }
})
import React, { useEffect, useState } from 'react'
import { Button, Keyboard, StyleSheet, Text, TextInput, TouchableWithoutFeedback, View } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import SearchTopTab from './SearchTopTab/searchTopTab'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useDispatch, useSelector } from 'react-redux';
import { fetchSearchUsers } from './../../../redux/actions/user.actions';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

export default function search({ navigation }) {
    const [search, setSearch] = useState('');
    const [topTab, setTopTab] = useState(false);

    const dispatch = useDispatch();
    const fetchUserss = (value) => {
        console.log('value ', value === "");
        if (value === "") {
            setTopTab(false)
        }
        setTopTab(true);
        dispatch(fetchSearchUsers(value));
    }

    useEffect(() => {
        dispatch(fetchSearchUsers(search));
    }, [])

    return (
        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
            <View style={styles.container}>
                <View style={styles.searchArea}>
                    <FontAwesome5 style={styles.searchIcon} name="search" size={20} color="black" />
                    <TextInput
                        style={{ ...styles.searchIcon, ...styles.searchText }}
                        placeholder='Search'
                        onChangeText={(value) => { setSearch(value); fetchUserss(value) }}
                    />

                </View>
                <View style={{ flex: 1 }}>
                    {(search) ? < SearchTopTab /> : null}
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    searchArea: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#f0f0f0',
        marginVertical: 10,
        marginHorizontal: 15,
        paddingLeft: 10,
        height: 40,
        borderRadius: 15,

    },
    searchIcon: {
        marginRight: 10,
    },
    searchText: {
        flex: 1,
    }
})

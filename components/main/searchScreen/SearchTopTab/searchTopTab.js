import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ListAccounts from '../listAccount/listAccount'
import ListTags from './../listTags/listTags';

const Tab = createMaterialTopTabNavigator();
export default function SearchTopTab() {
    return (
        <Tab.Navigator >
            <Tab.Screen name='Users' component={ListAccounts}
            // listeners={({ navigation }) => ({
            //     tabPress: event => {
            //         event.preventDefault();
            //         navigation.navigate('ListAccounts', { uid: auth().currentUser.uid });
            //     }
            // })}
            />
            <Tab.Screen name='Tags' component={ListTags} />
        </Tab.Navigator>
    )
}

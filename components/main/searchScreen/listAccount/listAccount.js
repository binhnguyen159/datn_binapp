import React, { useEffect } from 'react'
import { FlatList, Text, View, TouchableOpacity } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { Avatar } from 'react-native-paper'
import { topTabStyles } from '../../profileScreen/topTab/topTabStyles'
import { listStyles } from '../listStyles'
import firestore from '@react-native-firebase/firestore';
import { useRoute } from '@react-navigation/native';
import { FontAwesome5 } from 'react-native-vector-icons/FontAwesome5';
import { useSelector } from 'react-redux';


export default function ListAccounts({ navigation }) {

    let users = useSelector(state => state.userState.usersSearch);

    const chooseUser = (uid) => {
        navigation.navigate('Profile', { uid })
        console.log(uid);
    }

    return (
        <View style={topTabStyles.container}>
            <View>
                <FlatList
                    // keyExtractor={(item) => item?.id || Math.random().toString()}
                    key={(item) => item?.id}
                    data={users}
                    renderItem={({ item }) =>
                        item ?
                            (
                                <TouchableOpacity style={listStyles.content} onPress={() => chooseUser(item)}>
                                    <Avatar.Image source={{ uri: item.avatar }} />
                                    <View style={listStyles.info}>
                                        <Text style={{ fontWeight: 'bold' }}>{item.email}</Text>
                                        <Text style={{ fontWeight: '200' }}>{item.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            ) : null
                    }
                />
            </View>
        </View >
    )
}

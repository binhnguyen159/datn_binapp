import React from 'react'
import { Text, View } from 'react-native'
import { topTabStyles } from '../../profileScreen/topTab/topTabStyles'

export default function ListTags() {
    return (
        <View style={topTabStyles.container}>
            <Text>List Tags</Text>
        </View>
    )
}

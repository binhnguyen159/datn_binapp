import { StyleSheet } from "react-native";

export const listStyles = StyleSheet.create({
    searchArea: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#f0f0f0',
        marginVertical: 10,
        marginHorizontal: 15,
        paddingLeft: 10,
        height: 40,
        borderRadius: 15,

    },
    searchIcon: {
        marginRight: 10,
    },
    searchText: {
        flex: 1,
    },
    content: {
        marginVertical: 10,
        marginHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    info: {
        marginLeft: 20,
    },
})
import React, { useEffect, useState } from 'react'
import { FlatList } from 'react-native';
import { Text, View, TouchableOpacity, Button, Image } from 'react-native'
import ImagePicker from 'react-native-image-crop-picker'
import Video from 'react-native-video';


export default function add({ navigation, route }) {
    const [camera, setCamera] = useState(null);
    const [record, setRecord] = useState(null);
    const openCamera = () => {
        setCamera(null);
        setRecord(null);
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            console.log(image);
            setCamera(image.path);
        }).catch(err => console.log(err));
    }
    const openRecord = () => {
        setCamera(null);
        setRecord(null);
        ImagePicker.openCamera({
            mediaType: 'video',
        }).then(video => {
            setRecord(video.path);
        }).catch(err => console.log(err));
    }
    const openGallery = () => {
        setCamera(null);
        setRecord(null);
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            setCamera(image.path);
        }).catch(err => console.log(err));
    }

    return (
        <View style={{ flex: 1 }}>
            <Button title='Open camera' onPress={() => openCamera()} />
            <Button title='Open gallery' onPress={() => openGallery()} />
            <Button title='Open Record' onPress={() => openRecord()} />
            <View style={{ flex: 1, justifyContent: 'center' }}>

                {camera && <Image style={{ flex: 9 / 10, justifyContent: 'center' }} resizeMode='contain' source={{ uri: camera }} />}
                {record && <Video style={{ flex: 9 / 10, justifyContent: 'center' }} resizeMode='contain' source={{ uri: record }} />}
            </View>
            <View style={{ marginBottom: 10 }}>
                <Button title='Save' onPress={() => navigation.navigate('Save', { camera, record })} />
            </View>
        </View>
    )
}

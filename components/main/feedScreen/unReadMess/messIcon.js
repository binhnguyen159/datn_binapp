import React, { useEffect, useState } from 'react'
import { View } from 'react-native'
import { Badge } from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { feedStyles } from '../feedStyles';
import { TouchableOpacity } from 'react-native';
import auth from '@react-native-firebase/auth';
import { useDispatch, useSelector } from 'react-redux';
import { fetchMessages } from '../../../../redux/actions/messages.action';

export default function messIcon() {
    const [countMess, setCountMess] = useState(0);
    const [hasMessage, setHasMessage] = useState(false);
    const messages = useSelector(state => state.messState.messages);
    useEffect(async () => {
        if (messages.filter(item => item?.lastMess?.hasRead === false && item.lastMess.owner !== auth().currentUser.uid).length > 0) {
            setHasMessage(true)
        }
        if (messages.filter(item => item?.lastMess?.hasRead === false && item.lastMess.owner !== auth().currentUser.uid).length === 0) {
            setHasMessage(false)
        }
    }, [messages])

    const getCountComments = () => {
    }
    return (

        <View>
            <FontAwesome5 style={feedStyles.iconFooter} name='facebook-messenger' size={24} color='black' />
            {
                hasMessage ?
                    <Badge status='error' containerStyle={{ position: 'absolute', top: 0, right: 10 }} />
                    : null
            }
        </View>
    )
}

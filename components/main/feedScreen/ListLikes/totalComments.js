import React, { useEffect, useState } from 'react'
import { View } from 'react-native'
import { Badge } from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { feedStyles } from '../feedStyles';
import { TouchableOpacity } from 'react-native';

export default function totalComments({ uid, postID, navigation, item }) {
    const [countComments, setCountComments] = useState(0);
    const [update, setUpdate] = useState(0);

    useEffect(async () => {
        firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .collection('comments')
            .get()
            .then(snapshot => {
                const data = snapshot.docs.map(comment => {
                    return { id: comment.id }
                })
                setCountComments(data.length)
            });
    }, [])

    const getCountComments = () => {
        firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .collection('comments')
            .get()
            .then(snapshot => {
                const data = snapshot.docs.map(comment => {
                    return { id: comment.id }
                })
                setCountComments(data.length)
            });
    }
    return (
        <TouchableOpacity onPress={() => {
            navigation.navigate('Comment', { info: item });
        }}>
            <View>
                <FontAwesome5 style={feedStyles.iconFooter} name='comment' size={24} color='black' />
                {
                    countComments > 0 ?
                        <Badge value={countComments || null} status='primary' containerStyle={{ position: 'absolute', top: -4, right: -4 }} />
                        : null
                }
            </View>
        </TouchableOpacity>
    )
}

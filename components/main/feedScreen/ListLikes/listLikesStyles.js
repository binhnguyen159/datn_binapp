import { StyleSheet } from "react-native";

export const listLikesStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    searchArea: {
        flexDirection: "row",
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0.5,
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: 20,
        borderRadius: 15,
        borderColor: 'gray',
    },
    listLikes: {
        flex: 1,
    },
    searchElement: {
        marginLeft: 15
    }
})
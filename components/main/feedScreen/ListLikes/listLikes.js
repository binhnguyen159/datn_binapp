import React, { useEffect, useState } from 'react'
import { TextInput } from 'react-native';
import { FlatList } from 'react-native';
import { View, Text } from 'react-native'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import { listLikesStyles } from './listLikesStyles';
import firestore from '@react-native-firebase/firestore';
import { Avatar } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Header from '../../../../shared/header'

export default function listLikes({ route, navigation }) {
    const [users, setUsers] = useState([]);
    const [listUsers, setListUsers] = useState([]);
    const [show, setShow] = useState(false);
    const [search, setSearch] = useState('');
    useEffect(async () => {
        let users = [];
        for (let index = 0; index < route.params.listLikes.length; index++) {
            const data = await firestore()
                .collection('users')
                .doc(route.params.listLikes[index].id)
                .get()
                .then(snapshot => {
                    if (snapshot.exists) {
                        users.push({ id: snapshot.id, ...snapshot.data() });
                    }
                })
        }
        setUsers(users)
        setListUsers(users)
    }, [])

    const loadMore = () => {

    }

    const onChooseUser = (uid) => {
        console.log({ uid });
        navigation.navigate('Profile', { uid })
    }

    const onSearch = (value) => {
        if (value !== '') {
            const data = listUsers.filter(user => user.name.includes(value))
            console.log(data);
            setUsers(data)
        }
        else {
            setUsers(listUsers)
        }
    }

    const onClickSearch = () => {
        setShow(!show);
        setUsers(listUsers)
    }

    return (
        <View style={listLikesStyles.container}>
            <Header navigation={navigation} title='List likes' onPress={() => { onClickSearch() }} icon='search' />
            {
                show ?
                    <View style={listLikesStyles.searchArea} >
                        <FontAwesome5Icon name="search" size={24} color="gray" style={listLikesStyles.searchElement} />
                        <TextInput
                            placeholder='Search'
                            style={{ ...listLikesStyles.searchElement, width: '100%' }}
                            onChangeText={(value) => {
                                // setSearch(value);
                                onSearch(value);
                            }}
                        // value={search}
                        />
                    </View>
                    :
                    null
            }
            <View style={listLikesStyles.listLikes} >
                <FlatList
                    data={users}
                    keyExtractor={item => item.id}
                    onEndReached={() => {

                    }}
                    onEndReachedThreshold={0}
                    renderItem={({ item }) => (
                        <TouchableOpacity onPress={() => { onChooseUser(item) }}>
                            <View style={{ flexDirection: 'row', marginVertical: 5, marginLeft: 20 }} >
                                <Avatar.Image size={30} source={{ uri: item?.avatar }} />
                                <Text style={{ marginLeft: 10, fontSize: 16 }}>
                                    {item.name}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </View>
        </View>
    )
}

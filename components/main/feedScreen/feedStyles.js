import { Dimensions, StyleSheet } from "react-native";

export const feedStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    header: {
        paddingLeft: 20,
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: '#F0F0F0'
    },
    textContent: {
        fontSize: 18,
        fontWeight: 'bold',
        fontStyle: 'italic',
    },
    headerFeature: {
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    iconHeader: {
        marginRight: 20
    },
    feed: {
        flex: 1,
        marginTop: 10,
    },
    feedHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    },
    feedHeaderLeft: {
        marginLeft: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    feedHeaderRight: {
        marginRight: 10,
        paddingHorizontal: 10,
    },
    feedContent: {
        height: Dimensions.get('window').height / 2,
        marginTop: 10
    },
    feedImages: {
        width: '100%',
        // height: '100%',
        // flex: 1,
    },
    feedFooter: {
        marginTop: 10,
        marginLeft: 10,
    },
    feedFooterLeft: {
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    iconFooter: {
        marginRight: 15,
    },
    caption: {
        flexDirection: 'row',
    },
    listComments: {
        flexDirection: 'row',
    },
    comment: {
        flexDirection: 'row',
    },
    userName: {
        fontWeight: 'bold'
    },
    commentInput: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    loading: {
        marginTop: "100%",
    }
})
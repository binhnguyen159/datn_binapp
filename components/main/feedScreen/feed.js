import React, { useEffect, useState } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View, FlatList, ActivityIndicator, Modal } from 'react-native'
import { Avatar, } from 'react-native-paper'
import { Badge } from 'react-native-elements'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { feedStyles } from './feedStyles'
import { useDispatch, useSelector } from 'react-redux'
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import TotalComments from './ListLikes/totalComments'
import Video from 'react-native-video'
import feedDetail from './feedDetail/feedDetail'
import { Pages } from 'react-native-pages'
import MessIcon from './unReadMess/messIcon'
import { Dimensions } from 'react-native'
import FeedModal from './FeedModal'
import { clearDatas } from './../../../redux/actions/users.actions';

export default function feed({ navigation, route }) {
    const following = useSelector(state => state.userState.following);
    const usersLoaded = useSelector(state => state.usersState.usersLoaded);
    const feed = useSelector(state => state.usersState.feed)
    const [posts, setPosts] = useState([])
    const [page, setPage] = useState(0)
    const [loading, setLoading] = useState(false)
    const [modal, setModal] = useState(false);
    const [item, setItem] = useState({});
    useEffect(() => {
        setLoading(true)
        if (usersLoaded === following.length && following.length !== 0) {
            feed.sort((x, y) => {
                return y.score - x.score;
            })
            setPosts(feed)
            setLoading(false)
        };
        loadMore(page)
        return (() => {
            clearDatas();
        })
    }, [usersLoaded, feed]);

    const onLike = async (uid, postID, likesCount) => {
        likesCount = likesCount + 1;
        feed.map(element => {
            if (element.id === postID) {
                element.likesCount = likesCount;
                // element.score = oldScore;
            }
        });
        await firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .collection('likes')
            .doc(auth().currentUser.uid)
            .set({});

        const oldScore = await firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .get()
            .then(doc => {
                return doc.data().score;
            })

        await firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .update({
                likesCount: likesCount,
                score: oldScore + 1000
            })
    };

    const onUnLike = async (uid, postID, likesCount) => {
        likesCount = likesCount - 1;
        feed.map(element => {
            if (element.id === postID) {
                element.likesCount = likesCount;
            }
        });
        await firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .collection('likes')
            .doc(auth().currentUser.uid)
            .delete({})

        const oldScore = await firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .get()
            .then(doc => {
                return doc.data().score;
            })

        await firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .update({
                likesCount: likesCount,
                score: oldScore - 1000 < 0 ? 0 : oldScore - 1000,
            })
    };

    const onClickTotalLikes = async (uid, postID) => {
        const listLikes = await firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postID)
            .collection('likes')
            .get()
            .then(snapshot => {
                let likes = snapshot.docs.map(doc => {
                    return { id: doc.id };
                })
                return likes;
            })
        navigation.navigate('ListLikes', { listLikes })
    }

    const loadMore = (page) => {
        feed.slice(0, 5)
        // setPage(page + 1);
    }

    const handleCloseModal = () => {
        setModal(false);
    }

    return (
        <View style={feedStyles.container}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modal}
                onRequestClose={() => {
                    setModal(false);
                }}
            >
                <FeedModal post={item} onClose={handleCloseModal} />
            </Modal>
            <View style={feedStyles.header}>
                <Text style={feedStyles.textContent}>Bin</Text>
                <View style={feedStyles.headerFeature}>
                    <TouchableOpacity onPress={() => navigation.navigate('Add')}>
                        <FontAwesome5 style={feedStyles.iconHeader} name='plus-square' size={24} color='black' />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => navigation.navigate('ListMessage')}>
                        {/* <FontAwesome5 style={feedStyles.iconHeader} name='facebook-messenger' size={24} color='black' /> */}
                        <MessIcon />
                    </TouchableOpacity>
                </View>
            </View>
            {
                loading ? (
                    <View style={feedStyles.loading}>
                        <ActivityIndicator size="large" color="blue" animating={loading} />
                    </View>
                ) : (
                    <View
                        style={{ flex: 1 }}
                    >
                        <FlatList
                            // viewabilityConfig={viewabilityConfig}
                            numColumns={1}
                            horizontal={false}
                            data={feed}
                            onEndReached={() => { loadMore(page) }}
                            onEndReachedThreshold={0}
                            // onViewableItemsChanged={onViewableItemsChanged}
                            // keyExtractor={onViewRef.current}
                            renderItem={({ item }) => {
                                const totalImage = item.type == "mixed" ? item.downloadURL.length : 0;
                                return (
                                    <View style={feedStyles.feed}>
                                        <View style={feedStyles.feedHeader}>
                                            <View style={feedStyles.feedHeaderLeft}>
                                                <Avatar.Image style={{ marginRight: 10 }} size={40} source={{ uri: item?.user?.avatar }} />
                                                <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                                                    <TouchableOpacity onPress={() => {
                                                        navigation.navigate('Profile', { uid: { ...item.user, id: item?.user?.uid } })
                                                    }}>

                                                        <Text style={{ fontWeight: 'bold' }}>{item?.user?.name}</Text>
                                                    </TouchableOpacity>
                                                    <Text>{item.caption}</Text>
                                                </View>
                                            </View>

                                            <View style={feedStyles.feedHeaderRight}>
                                                <TouchableOpacity onPress={() => {
                                                    setModal(true);
                                                    setItem(item)
                                                }}>
                                                    <FontAwesome5 name='ellipsis-v' size={16} color='#545454' />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        {item.type === 'video' ?
                                            <Video style={
                                                { width: '100%', height: 400 }
                                            } resizeMode='cover' source={{ uri: item.downloadURL }} muted={true} />
                                            :
                                            <View />
                                        }
                                        {item.type === 'image' ?
                                            <Image style={
                                                { width: '100%', height: 400 }
                                            } resizeMode='cover' source={{ uri: item.downloadURL }} />
                                            :
                                            <View />
                                        }
                                        {
                                            item.type === 'mixed' ?
                                                <FlatList
                                                    horizontal={true}
                                                    showsHorizontalScrollIndicator={false}
                                                    pagingEnabled={true}
                                                    data={item.downloadURL}
                                                    keyExtractor={(item, index) => index.toString()}
                                                    renderItem={({ item, index }) => (
                                                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', width: Dimensions.get('window').width }}>
                                                            <Image source={{ uri: item }} style={{ width: Dimensions.get('window').width - 20, height: 400 }} />
                                                            {totalImage > 1 && <Text style={{ position: 'absolute', top: 0, right: 0, padding: 7, backgroundColor: 'rgba(169, 169, 169, 0.5)', fontWeight: "bold" }}>
                                                                {`${index + 1}/${totalImage}`}
                                                            </Text>}

                                                        </View>
                                                    )}
                                                />
                                                :
                                                null
                                        }

                                        <View style={feedStyles.feedFooter}>
                                            <View style={feedStyles.feedFooterLeft}>
                                                {
                                                    item.currentUserLike === true ?
                                                        (<TouchableOpacity onPress={() => { onUnLike(item?.user?.uid, item.id, item.likesCount) }}>
                                                            <AntDesign style={feedStyles.iconFooter} name='heart' size={24} color='red' />
                                                        </TouchableOpacity>)
                                                        :
                                                        (<TouchableOpacity onPress={() => { onLike(item?.user?.uid, item.id, item.likesCount) }}>
                                                            <AntDesign style={feedStyles.iconFooter} name='hearto' size={24} color='black' />
                                                        </TouchableOpacity>)
                                                }


                                                <TotalComments item={item} uid={item?.user?.uid} postID={item.id} navigation={navigation} />
                                                <TouchableOpacity onPress={() => {
                                                    navigation.navigate('Comment',
                                                        { info: item })
                                                }}>
                                                </TouchableOpacity>
                                            </View>
                                            <View>

                                                {
                                                    item.likesCount < 1 ?
                                                        <Text />
                                                        :
                                                        <TouchableOpacity onPress={() => onClickTotalLikes(item?.user?.uid, item.id)}>
                                                            <Text style={{ fontWeight: 'bold' }}>{item.likesCount} likes</Text>
                                                        </TouchableOpacity>
                                                }

                                            </View>
                                        </View>
                                    </View>
                                )
                            }
                            }
                        />
                    </View>
                )
            }


        </View>
    )

}

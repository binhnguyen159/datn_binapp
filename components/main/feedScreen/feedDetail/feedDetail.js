import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { Pages } from 'react-native-pages'


export default function feedDetail(data) {
    return data?.map((item) => {
        return (
            <Image source={{ uri: item }} style={{ width: '100%', height: 350 }} resizeMode='cover' />
        )
    })
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
})
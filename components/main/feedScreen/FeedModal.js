import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
export default function FeedModal({ onClose, post }) {
    const handleReport = () => {
        firestore()
            .collection("posts")
            .doc(post.user.uid)
            .collection("userPosts")
            .doc(post.id)
            .update({
                report: true
            });
    }

    const handleDelete = () => {
        firestore()
            .collection("posts")
            .doc(post.user.uid)
            .collection("userPosts")
            .doc(post.id)
            .delete();
    }
    console.log("post", post)
    return (
        <TouchableOpacity
            style={styles.root}
            onPress={() => onClose()}
        >
            <View style={styles.modalContent}>
                <View style={styles.modalRow}>
                    <TouchableOpacity
                        onPress={handleReport}
                        style={styles.modalItem}>
                        <View style={styles.modalItemContent}>
                            <MaterialIcons name="report" size={24} color="gray" />
                            <Text>Report</Text>
                        </View>
                    </TouchableOpacity>
                    {post.user.uid === auth().currentUser.uid && (
                        <TouchableOpacity
                            onPress={handleDelete}
                            style={styles.modalItem}>
                            <View style={styles.modalItemContent}>
                                <MaterialIcons name="delete" size={24} color="gray" />
                                <Text>Delete</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        height: "100%",
        width: "100%",
        backgroundColor: "rgba(0, 0, 0, 0)"
    },
    modalContent: {
        height: 100,
        width: "100%",
        position: 'absolute',
        bottom: 0,
        backgroundColor: "white",
        borderColor: '#f2f2f2',
        borderWidth: 1,
        borderRadius: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    modalRow: {
        flex: 1,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    modalItem: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    modalItemContent: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    }
})

import React, { useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
export default function OptionsComment({ onClose, comment, info, onRemove }) {

    // useEffect(() => {
    //     getHide();
    // }, [])

    const handleReport = async () => {
        await firestore()
            .collection("posts")
            .doc(info.user.uid)
            .collection("userPosts")
            .doc(info.id)
            .update({
                report: true
            });
        await firestore()
            .collection("posts")
            .doc(info.user.uid)
            .collection("userPosts")
            .doc(info.id)
            .collection("comments")
            .doc(comment.id)
            .update({
                report: true
            });
    }

    const handleDelete = () => {
        firestore()
            .collection("posts")
            .doc(info.user.uid)
            .collection("userPosts")
            .doc(info.id)
            .collection("comments")
            .doc(comment.id)
            .delete();
    }



    const handleHide2 = async () => {
        await firestore()
            .collection("posts")
            .doc(info.user.uid)
            .collection("userPosts")
            .doc(info.id)
            .collection("comments")
            .doc(comment.id)
            .collection("hides")
            .add({ user: auth().currentUser.uid });

        onRemove()
    }

    const handleHide = async () => {
        await firestore()
            .collection("posts")
            .doc(info.user.uid)
            .collection("userPosts")
            .doc(info.id)
            .collection("comments")
            .doc(comment.id)
            .update({ hide: true });
    }
    console.log(comment);
    return (
        <TouchableOpacity
            style={styles.root}
            onPress={() => onClose()}
        >
            <View style={styles.modalContent}>
                <View style={styles.modalRow}>
                    <TouchableOpacity
                        onPress={handleReport}
                        style={styles.modalItem}>
                        <View style={styles.modalItemContent}>
                            <MaterialIcons name="report" size={24} color="gray" />
                            <Text>Report</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={handleHide2}
                        style={styles.modalItem}>
                        <View style={styles.modalItemContent}>
                            <MaterialCommunityIcons name="eye-off" size={24} color="gray" />
                            <Text>Hide</Text>
                        </View>
                    </TouchableOpacity>
                    {(comment.creator === auth().currentUser.uid || info.user.id === auth().currentUser.uid) && (
                        <TouchableOpacity
                            onPress={handleHide}
                            style={styles.modalItem}>
                            <View style={styles.modalItemContent}>
                                <MaterialCommunityIcons name="table-eye-off" size={24} color="gray" />
                                <Text>Hide for all</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                    {comment.creator === auth().currentUser.uid && (
                        <TouchableOpacity
                            onPress={handleDelete}
                            style={styles.modalItem}>
                            <View style={styles.modalItemContent}>
                                <MaterialIcons name="delete" size={24} color="gray" />
                                <Text>Delete</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        height: "100%",
        width: "100%",
        backgroundColor: "rgba(0, 0, 0, 0)"
    },
    modalContent: {
        height: 100,
        width: "100%",
        position: 'absolute',
        bottom: 0,
        backgroundColor: "white",
        borderColor: '#f2f2f2',
        borderWidth: 1,
        borderRadius: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    modalRow: {
        flex: 1,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    modalItem: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    modalItemContent: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    }
})

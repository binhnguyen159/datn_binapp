import { StyleSheet } from "react-native";

export const commentStyles = StyleSheet.create({
    commentInput: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderTopWidth: 0.5,
        paddingHorizontal: 15,
        // margin: 5,
        // height: '70%',
    },
})
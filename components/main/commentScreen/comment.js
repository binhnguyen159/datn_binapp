import React, { useEffect, useState } from 'react'
import { Text, View, TextInput, TouchableOpacity, FlatList, Modal, Alert, TouchableWithoutFeedback } from 'react-native'
import { Avatar } from 'react-native-paper'
import { commentStyles } from './commentStyle'
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { Keyboard } from 'react-native'
import Feather from 'react-native-vector-icons/Feather';
import { useDispatch, useSelector } from 'react-redux';
import { fetchComments } from './../../../redux/actions/comments.action';
import OptionsComment from './optionsComment/OptionsComment'

export default function comment({ route }) {
    let listHide = [];
    const [listUserHide, setListUserHide] = useState([]);
    const [comment, setComment] = useState('');
    const [item, setItem] = useState({});
    const [filter, setFilter] = useState(true);
    const listComments = useSelector(state => state.commentsState.comments);
    const dispatch = useDispatch()
    const [modal, setModal] = useState(false);
    useEffect(async () => {
        await dispatch(fetchComments(route.params.info.user.uid, route.params.info.id));
        await getHide()
    }, [filter]);

    const handleCloseModal = () => {
        setModal(false);
    }

    console.log(route.params.info);

    const getHide = async () => {
        await listComments.map(async value => {
            await firestore()
                .collection("posts")
                .doc(route.params.info.user.uid)
                .collection("userPosts")
                .doc(route.params.info.id)
                .collection("comments")
                .doc(value.id)
                .collection("hides")
                .get()
                .then(snap => {
                    if (snap.size > 0)
                        return snap.docs.map(item => {
                            if (item.data().user === auth().currentUser.uid) {
                                listHide.push(value.id)
                                setListUserHide(listHide)
                            }

                        })
                })
        })
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modal}
                onRequestClose={() => {
                    setModal(false);
                }}
            >
                <OptionsComment comment={item} info={route.params.info} onClose={handleCloseModal} onRemove={() => { setFilter(!filter) }} />
            </Modal>

            <TouchableWithoutFeedback
                onPress={() => {
                    setModal(false)
                }}
            >
                <View
                    style={{ flex: 10, display: "flex", flexDirection: "column", justifyContent: "flex-start" }}
                >
                    <View>
                        <Text>{route?.params.info.caption}</Text>
                    </View>
                    <FlatList
                        keyExtractor={item => item.id}
                        data={listComments}
                        renderItem={({ item }) => {
                            if (listUserHide.includes(item.id) || item.hide)
                                return null
                            else {
                                return (
                                    <TouchableOpacity
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'flex-start',
                                            alignItems: 'center',
                                            marginVertical: 5,
                                            marginHorizontal: 10,
                                            marginVertical: 10,
                                        }}
                                        delayLongPress={1000}
                                        onLongPress={() => {
                                            setModal(true);
                                            setItem(item)
                                        }}
                                    >
                                        <View style={{
                                            flexDirection: 'row',
                                            justifyContent: 'flex-start',
                                            alignItems: 'center',
                                            marginVertical: 5,
                                            marginHorizontal: 10,
                                            marginVertical: 10,
                                        }}>
                                            <Avatar.Image size={40} source={{ uri: item?.user?.avatar }} />
                                            <View style={{
                                                flex: 1,
                                                borderBottomRightRadius: 10,
                                                borderTopRightRadius: 10,
                                                borderTopLeftRadius: 10,
                                                backgroundColor: '#EFEFEF',
                                                paddingHorizontal: 10,
                                                marginRight: 20,
                                            }}>
                                                <Text style={{ fontWeight: 'bold', marginLeft: 10 }}>{item?.user?.name}</Text>
                                                <Text style={{ marginLeft: 10 }}>{item.content}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }
                        }}
                    />
                </View>
            </TouchableWithoutFeedback>

            <View style={{ flex: 1 }}>
                <View style={commentStyles.commentInput}>
                    <TextInput onChangeText={(value) => { setComment(value) }} style={{ flex: 1, paddingLeft: 20 }} multiline={true} placeholder='Add a comment...' value={comment} />
                    <TouchableOpacity
                        onPress={async () => {
                            if (comment) {
                                console.log('asdasd');
                                const oldScore = await firestore()
                                    .collection('posts')
                                    .doc(route.params.info.user.uid)
                                    .collection('userPosts')
                                    .doc(route.params.info.id)
                                    .get()
                                    .then(doc => {
                                        return doc.data().score;
                                    })
                                await firestore()
                                    .collection('posts')
                                    .doc(route.params.info.user.uid)
                                    .collection('userPosts')
                                    .doc(route.params.info.id)
                                    .update({
                                        score: oldScore + 1,
                                    })
                                await firestore()
                                    .collection('posts')
                                    .doc(route.params.info.user.uid)
                                    .collection('userPosts')
                                    .doc(route.params.info.id)
                                    .collection('comments')
                                    .add({
                                        content: comment,
                                        creation: firestore.FieldValue.serverTimestamp(),
                                        creator: auth().currentUser.uid,
                                        report: false
                                    }).then(() => {

                                        setComment('');
                                        Keyboard.dismiss();
                                    })
                            }
                            // dispatch(clearListComments())
                        }}
                        style={{ marginHorizontal: 30 }}>
                        <Feather name='send' size={24} color='black' />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

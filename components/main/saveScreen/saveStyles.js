import { StyleSheet } from "react-native";

export const saveStyles = StyleSheet.create({
    borderElement: {
        flexDirection: 'row',
        alignItems: 'center',
        borderTopWidth: 0.5,
        borderColor: 'gray',
    },
    element: {
        marginLeft: 15,
        paddingVertical: 5
    }
})
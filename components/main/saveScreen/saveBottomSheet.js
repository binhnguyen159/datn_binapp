import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { saveStyles } from './saveStyles'

export default function saveBottomSheet() {
    return (
        <View style={{ marginTop: 30 }} >
            {/* <Button title='Post' onPress={() => { post().then(() => setPicker([])) }} /> */}
            {/* <Button title='Open camera' />
                <Button title='Open gallery' />
                <Button title='Open Record' /> */}
            <TouchableOpacity style={saveStyles.borderElement} onPress={() => openCamera()}>
                <FontAwesome5 style={saveStyles.element} name="camera" size={24} color="#25AFF3" />
                <Text style={saveStyles.element} >Camera</Text>
            </TouchableOpacity>
            <TouchableOpacity style={saveStyles.borderElement} onPress={() => openRecord()} >
                <FontAwesome5 style={saveStyles.element} name="video" size={24} color="#FFCA28" />
                <Text style={saveStyles.element} >Record</Text>
            </TouchableOpacity>
            <TouchableOpacity style={saveStyles.borderElement} onPress={() => openGallery()}>
                <FontAwesome5 style={saveStyles.element} name="images" size={24} color="#08E14C" />
                <Text style={saveStyles.element} >Pick Up Photo From Gallery</Text>
            </TouchableOpacity>
            <TouchableOpacity style={saveStyles.borderElement} >
                <MaterialIcons style={saveStyles.element} name="video-library" size={24} color="#FF0000" />
                <Text style={saveStyles.element} >Pick Up Video From Gallery</Text>
            </TouchableOpacity>
        </View>
    )
}

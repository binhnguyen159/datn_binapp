import React, { useEffect, useRef, useState } from 'react'
import { Text, Dimensions, FlatList, Image, Keyboard, TextInput, View, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import firestore from '@react-native-firebase/firestore'
import storage from '@react-native-firebase/storage'
import auth from '@react-native-firebase/auth'
import { useDispatch } from 'react-redux';
import { fetchUserAddPosts } from '../../../redux/actions/users.actions';
import ImagePicker from 'react-native-image-crop-picker'
import Video from 'react-native-video';
import Header from './../../../shared/header'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { saveStyles } from './saveStyles';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';


export default function save({ navigation, route }) {
    const dispatch = useDispatch();
    const [caption, setCaption] = useState('');
    const [camera, setCamera] = useState({});
    const [picker, setPicker] = useState([]);
    const [type, setType] = useState('text');
    const [isShow, setIsShow] = useState(true);

    useEffect(() => {
        if (route.params != undefined) {
            if (route.params.type === 'camera') {
                openCamera()
            }
            else if (route.params.type === 'video') {
                openRecord()
            }
        }
    }, [route.params])

    const openCamera = () => {
        setCamera({});
        // setRecord({});
        setType('image');
        ImagePicker.openCamera({
            cropping: true,
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').width
        }).then(image => {
            console.log(image.path);
            setType('image');
            setCamera({ path: image.path, width: image.width, height: image.height });
        }).catch(err => {
            setType('text');
            console.log(err);
        });
    }
    const openRecord = () => {
        setCamera({});
        setType('video');
        ImagePicker.openCamera({
            mediaType: 'video',
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').width
        }).then(video => {
            setType('video');
            console.log(video);
            setCamera({ path: video.path, width: video.width, height: video.height });
        }).catch(err => {
            setType('text');
            console.log(err)
        });
    }
    const openGallery = () => {
        setCamera({});

        ImagePicker.openPicker({
            multiple: true,
            mediaType: "photo",
        }).then(file => {
            setType('mixed')
            setPicker(file);
        }).catch(err => {
            setType('text');
            console.log(err)
        });
    }

    const post = async () => {
        if (type === 'image' || type === 'video') {
            try {
                const uri = camera.path;
                const childPath = `post/${auth().currentUser.uid}/${Math.random().toString(36)}`;
                console.log(childPath);

                const response = await fetch(uri);
                const blob = await response.blob();

                const task = storage().ref().child(childPath).put(blob);

                const taskProgress = (snapshot) => {
                    console.log(`transferred: ${snapshot.bytesTransferred}`);
                }
                const taskCompleted = (snapshot) => {
                    task.snapshot.ref.getDownloadURL().then((snapshot) => {
                        savePostData(snapshot);
                        console.log('taskCompleted ', snapshot);
                    })
                }
                const taskError = snapshot => {
                    console.log(snapshot)
                }

                await task.on("state_changed", taskProgress, taskError, taskCompleted);
                setCaption("")
            } catch (error) {
                console.log(error);
            }
        }
        else if (type === 'text') {
            savePostData(null).then(() => setCaption(""));
        }
        else if (type === 'mixed') {
            try {
                let data = [];
                for (let index = 0; index < picker.length; index++) {
                    const uri = picker[index].path
                    const childPath = `post/${auth().currentUser.uid}/${Math.random().toString(36)}`;
                    console.log(childPath);

                    const response = await fetch(uri);
                    const blob = await response.blob();

                    const task = storage().ref().child(childPath).put(blob);

                    const taskProgress = (snapshot) => {
                        console.log(`transferred: ${snapshot.bytesTransferred}`);
                    }
                    const taskCompleted = (snapshot) => {
                        task.snapshot.ref.getDownloadURL().then(async (snapshot) => {
                            await data.push(snapshot)
                            if (index === (picker.length - 1)) {
                                console.log('data21332131 ', data);
                                savePostData(data);
                                // setPicker([]);
                            }
                        })
                    }
                    const taskError = snapshot => {
                        console.log(snapshot)
                    }
                    await task.on("state_changed", taskProgress, taskError, taskCompleted);
                    // if (index === (picker1.length - 1)) {
                    //     console.log('save ', save);
                    //     savePostData(save);
                    //     break;
                    // }
                    setCaption("")
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    const savePostData = async (downloadURL) => {
        const date = firestore.FieldValue.serverTimestamp()
        await firestore()
            .collection('posts')
            .doc(auth().currentUser.uid)
            .collection("userPosts")
            .add({
                downloadURL,
                caption,
                likesCount: 0,
                creation: date,
                type,
                score: Date.now() / 1000,
                report: false,
            })
            .then(async (docAdd) => {
                const data = await firestore()
                    .collection('users')
                    .doc(auth().currentUser.uid)
                    .get()
                    .then(doc => {
                        if (doc.exists) {
                            const id = doc.id;
                            const data = doc.data()

                            const newPosts = {
                                id: docAdd.id,
                                downloadURL,
                                caption,
                                likesCount: 0,
                                creation: date,
                                type: type,
                                user: { uid: id, ...data },
                                score: Date.now() / 1000,
                                report: false,
                            }
                            dispatch(fetchUserAddPosts(newPosts))
                            return { ...newPosts, user: { uid: id, ...data } }
                        }
                    })
                navigation.navigate('Feed', { newPost: data });
            })
    };

    const headerBottomSheet = () => {
        return (
            <TouchableOpacity
                style={{
                    alignItems: 'center',
                    backgroundColor: 'white',
                    paddingVertical: 5,
                    borderTopRightRadius: 15,
                    borderTopLeftRadius: 15,
                }}
                onPress={() => closeCreateBottomSheet()}
            >

                <FontAwesome5 name="angle-down" size={24} color="black" />
            </TouchableOpacity>
        )
    }

    const saveBottomSheet = () => {
        return (
            <View style={{ backgroundColor: 'white' }}>
                {/* <Button title='Post' onPress={() => { post().then(() => setPicker([])) }} /> */}
                {/* <Button title='Open camera' />
                    <Button title='Open gallery' />
                    <Button title='Open Record' /> */}
                <TouchableOpacity style={{ ...saveStyles.borderElement }} onPress={() => {
                    openCamera();
                    closeCreateBottomSheet();
                    setIsShow(true);
                }}>
                    <FontAwesome5 style={saveStyles.element} name="camera" size={24} color="#25AFF3" />
                    <Text style={saveStyles.element} >Camera</Text>
                </TouchableOpacity>
                <TouchableOpacity style={saveStyles.borderElement} onPress={() => {
                    openRecord();
                    closeCreateBottomSheet();
                    setIsShow(true);
                }} >
                    <FontAwesome5 style={saveStyles.element} name="video" size={24} color="#FFCA28" />
                    <Text style={saveStyles.element} >Record</Text>
                </TouchableOpacity>
                <TouchableOpacity style={saveStyles.borderElement} onPress={() => {
                    openGallery();
                    closeCreateBottomSheet();
                    setIsShow(true);
                }}>
                    <FontAwesome5 style={saveStyles.element} name="images" size={24} color="#08E14C" />
                    <Text style={saveStyles.element} >Pick Up Photo From Gallery</Text>
                </TouchableOpacity>
            </View>
        )
    }
    const openCreateBottomSheet = () => {
        sheetRef.current.snapTo(0)
    }
    const closeCreateBottomSheet = () => {
        sheetRef.current.snapTo(1)
    }

    const sheetRef = useRef(null);
    const fall = new Animated.Value(1);

    const unChooseImage = (link) => {
        const data = picker.filter(value => value.path !== link);
        setPicker(data);
    }
    const headerPost = () => {


    }
    return (
        <View style={{ flex: 1 }}>
            <Animated.View style={{
                flex: 1,
                opacity: Animated.add(0.5, Animated.multiply(fall, 1.0)),
            }}>
                <Header
                    navigation={navigation}
                    title='Add post'
                    icon='angle-right'
                    onPress={() => {
                        if (!caption && type === 'text') {
                            console.log(123);
                            return;
                        }
                        post().then(() => {
                            setPicker([]);
                            setCaption("");
                        })
                    }}
                />


                <View style={{ justifyContent: 'center' }}>
                    <View style={{
                        borderWidth: 1,
                        borderColor: 'black',
                        borderRadius: 10,
                        margin: 10,
                    }}>
                        <TextInput
                            multiline={true}
                            style={{ paddingVertical: 30, maxHeight: 150 }}
                            placeholder='Write a caption...'
                            multiline={true}
                            onChangeText={(data) => {
                                setCaption(data);
                            }}
                            value={caption}
                        />
                    </View>
                </View>
                {type === 'image' && <Image style={{ width: camera.width, height: camera.height }} resizeMode='cover' source={{ uri: camera.path }} />}
                {type === 'video' && <Video style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').width }} resizeMode='cover' source={{ uri: camera.path }} />}
                <TouchableWithoutFeedback onPress={() => {
                    Keyboard.dismiss();
                    closeCreateBottomSheet();
                    setIsShow(true);
                }}>
                    <View style={{ height: 700, maxHeight: 700 }}>
                        {type === 'mixed' &&
                            <FlatList
                                numColumns={3}
                                horizontal={false}
                                data={picker}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <View style={{
                                        width: Dimensions.get('window').width / 3,
                                        height: Dimensions.get('window').width / 3,
                                    }}>

                                        <Image style={{
                                            flex: 1, aspectRatio: 1 / 1, margin: 1
                                        }}
                                            resizeMode='cover' source={{ uri: item.path }} />
                                        <TouchableOpacity
                                            style={{ position: 'absolute', top: 0, right: 0, }}
                                            onPress={() => unChooseImage(item.path)}>
                                            <Text style={{ padding: 7, backgroundColor: 'rgba(169, 169, 169, 0.5)' }}>x</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity></TouchableOpacity>
                                    </View>
                                )}
                            />
                        }
                    </View>
                </TouchableWithoutFeedback>
                {isShow ? (
                    <TouchableOpacity
                        style={{ position: 'absolute', bottom: 0, width: '100%' }}
                        onPress={() => {
                            openCreateBottomSheet();
                            setIsShow(false);
                        }}
                    >
                        <View style={{
                            borderTopWidth: 0.5,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingVertical: 7
                        }}>
                            <View>
                                <Text style={{ marginLeft: 10, fontWeight: '500' }}>Add to your Post</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <FontAwesome5 style={{ marginRight: 10 }} name="camera" size={24} color="#25AFF3" />
                                <FontAwesome5 style={{ marginRight: 10 }} name="video" size={24} color="#FFCA28" />
                                <FontAwesome5 style={{ marginRight: 10 }} name="images" size={24} color="#08E14C" />
                            </View>
                        </View>
                    </TouchableOpacity>
                ) :
                    null}
                {/* <Button style={{ position: 'absolute', bottom: 0 }} title="click"  /> */}
            </Animated.View>
            <BottomSheet
                ref={sheetRef}
                snapPoints={[150, 0]}
                renderContent={saveBottomSheet}
                renderHeader={headerBottomSheet}
                initialSnap={1}
                callbackNode={fall}
                enabledGestureInteraction={true}
                onCloseEnd={() => { setIsShow(true) }}
            />
        </View >
    )
}

import React, { useState, useEffect } from 'react'
import { View, Text, TextInput, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import { signInStyles } from './loginStyles'
import FlatButton from '../../../shared/button'
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { LoginManager, AccessToken } from 'react-native-fbsdk-next';
import { saveTokenDevice } from '../../../utils/notification';
import { Formik, useFormik } from 'formik';
import { loginSchema } from '../../../utils/loginValidate';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'


export default function Login({ navigation }) {
    const [icon, setIcon] = useState('eye-slash');
    const [secure, setSecure] = useState(true);
    const [incorrect, setIncorrect] = useState("");

    const changeImage = () => {
        setIcon(icon === 'eye-slash' ? 'eye' : 'eye-slash');
        setSecure(!secure);
    }

    const enterEmail = (data) => {
        setEmail(data);
    }
    const enterPassword = (data) => {
        setPassword(data);
    }

    const createAccount = async (user) => {
        const data = await firestore()
            .collection('users')
            .doc(user.uid)
            .get()
            .then(snapshot => {
                if (snapshot.exists) {
                    return snapshot.data().tokens
                }
            });
        await saveTokenDevice()
            .then(token => {
                firestore().collection("users")
                    .doc(user.uid)
                    .update({
                        tokens: [...data, token],
                    })
                auth().currentUser.updateProfile({
                    displayName: user.displayName,
                }).then(function () {
                    console.log('success');
                }).catch(function (error) {
                    console.log('err login social: ', error);
                });

            });
    }

    const login = (email, password) => {
        if (email === '' || password === '') {
            console.log('Please fill the email and password');
            return;
        }

        auth().signInWithEmailAndPassword(email, password)
            .then(() => {
                createAccount(auth().currentUser);
            })
            .catch(err => {
                console.log(err.code, err.message);
                if (err.code == "auth/wrong-password" || err.code == "auth/user-not-found")
                    setIncorrect("Email or password was incorrect")
                if (err.code == "auth/too-many-requests")
                    setIncorrect("Input wrong too many, please try later")
            });

    }

    const loginFacebook = async () => {
        try {
            // Attempt login with permissions
            const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

            if (result.isCancelled) {
                throw 'User cancelled the login process';
            }

            // Once signed in, get the users AccesToken
            const data = await AccessToken.getCurrentAccessToken();

            if (!data) {
                throw 'Something went wrong obtaining access token';
            }

            // Create a Firebase credential with the AccessToken
            const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

            // Sign-in the user with the credential
            return auth().signInWithCredential(facebookCredential)
                .then((response) => { createAccount(response.user) })
                .catch(error => console.log('error login facebook: ', error));
        } catch (error) {
            console.log('error:\n', error);
        }
    }

    async function onGoogleButtonPress() {
        try {
            // Get the users ID token
            const { idToken } = await GoogleSignin.signIn();

            // // Create a Google credential with the token
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);

            // // Sign-in the user with the credential
            auth().signInWithCredential(googleCredential)
                .then(() => { createAccount(auth().currentUser) })
                .catch(error => console.log(error));
        } catch (error) {
            console.log(error);
        }
    }


    return (
        <View style={signInStyles.background}>
            <ScrollView>
                <View style={signInStyles.image}>
                    <Image style={{ width: 200, height: 200 }} source={require('./../../../image/logo.png')} />
                </View>
                <Formik
                    initialValues={{
                        email: "",
                        password: "",
                    }}
                    validationSchema={loginSchema}
                    onSubmit={(values) => {
                        setIncorrect("");
                        login(values.email, values.password);

                    }}
                >
                    {(formikProps) => (
                        <View>
                            <View style={signInStyles.text}>
                                <View style={signInStyles.line}>
                                    <FontAwesome5 name="user-alt" size={24} color="black" style={signInStyles.icon} />
                                    {/* <Image resizeMode="contain" style={signInStyles.icon} source={require('../../../image/icons/user.png')} size={24} /> */}
                                    <TextInput
                                        style={signInStyles.input}
                                        placeholder="Email"
                                        onChangeText={formikProps.handleChange("email")}
                                        require={true}
                                        value={formikProps.values.email}
                                        onBlur={formikProps.handleBlur("email")}
                                    />
                                </View>
                                <Text style={signInStyles.error}>{formikProps.touched.email && formikProps.errors.email}</Text>
                                <View style={signInStyles.line}>
                                    <FontAwesome5 name="lock" size={24} color="black" style={signInStyles.icon} />
                                    {/* <Image resizeMode="contain" style={signInStyles.icon} source={require('../../../image/icons/pass.png')} size={24} onp /> */}
                                    <TextInput
                                        style={signInStyles.input}
                                        placeholder="Password"
                                        onChangeText={formikProps.handleChange("password")}
                                        secureTextEntry={secure}
                                        // inlineImageLeft='search_icon'
                                        require={true}
                                        value={formikProps.values.password}
                                        onBlur={formikProps.handleBlur("password")}
                                    />
                                    <TouchableOpacity onPress={() => changeImage()}>
                                        <FontAwesome5 name={icon} size={20} color="black" style={signInStyles.icon} />
                                        {/* <Image resizeMode="contain" style={signInStyles.icon} source={icon === 'eye-slash' ? require('../../../image/icons/eye-slash.png') : require('../../../image/icons/eye.png')} /> */}
                                    </TouchableOpacity >
                                </View >
                                <Text style={signInStyles.error}>{formikProps.touched.password && formikProps.errors.password}</Text>
                            </View >
                            <FlatButton
                                title='Sign in'
                                onPress={() => formikProps.handleSubmit()}
                            />
                            <Text style={{ ...signInStyles.error, textAlign: "center", top: 0, marginTop: 5 }}>{incorrect}</Text>
                        </View>
                    )}
                </Formik>

                <View style={signInStyles.or}>
                    <Text style={{ flex: 1, height: 1, backgroundColor: '#7F7F7F' }}></Text>
                    <Text style={{ color: '#7F7F7F' }}>   Or sign in with   </Text>
                    <Text style={{ flex: 1, height: 1, backgroundColor: '#7F7F7F' }}></Text>
                </View>
                <View style={signInStyles.socialOnline}>
                    <TouchableOpacity style={signInStyles.socialButton} onPress={() => loginFacebook()}>
                        <Image style={signInStyles.logo} source={require('../../../image/facebook.jpg')} />
                    </TouchableOpacity>

                    <TouchableOpacity style={signInStyles.socialButton}
                        onPress={() => onGoogleButtonPress()}
                    >
                        <Image style={signInStyles.logo} source={require('../../../image/google.jpg')} />
                    </TouchableOpacity>

                    {/* <TouchableOpacity style={signInStyles.socialButton}>
                        <Image style={signInStyles.logo} source={require('../../../image/instagram.jpg')} />
                    </TouchableOpacity> */}
                </View>
                <View style={signInStyles.content}>
                    <Text>Don't have an account? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <Text style={signInStyles.textContent}>Register now</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView >
        </View >
    )
}

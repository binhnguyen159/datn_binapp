import { StyleSheet } from 'react-native'

export const signInStyles = StyleSheet.create({
    background: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#FFFFFF",
        justifyContent: "center"
    },
    image: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 80,
        marginBottom: 60,
    },
    text: {
        flex: 3,
        marginBottom: 30
    },
    input: {
        flex: 1,
    },
    icon: {
        paddingHorizontal: 10,
    },
    line: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#000',
        height: 40,
        borderRadius: 5,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 10
    },
    or: {
        // flex: 1,
        marginTop: 50,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        marginVertical: 30,
    },
    socialOnline: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    socialButton: {
        width: 50,
        height: 50
    },
    logo: {
        width: 50,
        height: 50
    },
    content: {
        // flex: 1,
        flexDirection: 'row',
        marginVertical: 30,
        justifyContent: 'flex-end',
    },
    textContent: {
        textAlign: 'right',
        textDecorationLine: 'underline',
        marginRight: 35,
        alignItems: 'center',
        justifyContent: 'center'
    },
    error: {
        textAlign: "left",
        color: "red",
        marginLeft: 15,
        top: -12,
        fontWeight: "bold"
    }
})
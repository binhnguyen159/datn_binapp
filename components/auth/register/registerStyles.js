import { StyleSheet } from "react-native";

export const registerStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    open: {
        // flex: 1,
        paddingVertical: 70,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    title: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 30,
    },
    content: {
        // flex: 2
        paddingBottom: 150,
    },
    text: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'gray',
        margin: 10,
        borderRadius: 10,
        marginBottom: 30
    },
    textContent: {
        flex: 1,
        paddingHorizontal: 20,
    },
    icon: {
        paddingHorizontal: 10,
    },
    error: {
        textAlign: "left",
        color: "red",
        marginLeft: 20,
        fontWeight: "bold",
        top: -20,
    },
})
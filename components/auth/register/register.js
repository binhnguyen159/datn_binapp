import React, { useState } from 'react'
import { View, TextInput, Text, ActivityIndicator, ScrollView } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { registerStyles } from './registerStyles'
import FlatButton from '../../../shared/button';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { saveTokenDevice } from '../../../utils/notification';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { Formik } from 'formik';
import { registerSchema } from './../../../utils/register';



export default function SignUp() {
    const [loading, setLoading] = useState(false);
    const [icon, setIcon] = useState('eye-slash');
    const [iconConfirm, setIconConfirm] = useState('eye-slash');
    const [secure, setSecure] = useState(true);
    const [secureConfirm, setSecureConfirm] = useState(true);
    const [error, setError] = useState("");


    const changeImage = () => {
        setIcon(icon === 'eye-slash' ? 'eye' : 'eye-slash');
        setSecure(!secure);
    }
    const changeImageConfirm = () => {
        setIconConfirm(iconConfirm === 'eye-slash' ? 'eye' : 'eye-slash');
        setSecureConfirm(!secureConfirm);
    }

    const onSignUp = async (name, email, password) => {
        console.log(name, email, password);
        saveTokenDevice()
            .then(token => {
                auth().createUserWithEmailAndPassword(email, password)
                    .then(() => {
                        firestore().collection("users")
                            .doc(auth().currentUser.uid)
                            .set({
                                name: name,
                                email: email,
                                avatar: "https://firebasestorage.googleapis.com/v0/b/instagram-app-f3d8d.appspot.com/o/avatars%2Fconan.jpg?alt=media&token=76c324b8-84f0-4e4a-beeb-6cb781931ec3",
                                tokens: [token],
                            })
                        setLoading(false)
                        auth().currentUser.updateProfile({ displayName: name })
                            .catch(function (error) {
                                console.log(error.code);
                            });
                    })
                    .catch(error => {
                        console.log(error.code, error.message);
                        if (error.code == "auth/email-already-in-use")
                            setError("This email have already exists")
                    });
            });

    }

    return (
        <View style={registerStyles.container}>
            <ScrollView >
                <View style={registerStyles.open}>
                    <Text style={registerStyles.title}>Binleaf</Text>
                </View>
                <Formik
                    initialValues={{
                        name: "",
                        email: "",
                        password: "",
                        confirmPassword: "",
                    }}
                    validationSchema={registerSchema}
                    onSubmit={(values) => {
                        setError("");
                        if (values.password != values.confirmPassword)
                            setError("The Password filed and Confirm password field must be same")
                        else {
                            onSignUp(values.name, values.email, values.password)
                            setLoading(true)
                        }
                    }}
                >
                    {(formikProps) => (
                        <View style={registerStyles.content}>
                            <View style={registerStyles.text}>
                                <TextInput style={registerStyles.textContent}
                                    placeholder="Name"
                                    onChangeText={formikProps.handleChange("name")}
                                    value={formikProps.values.name}
                                    onBlur={formikProps.handleBlur("name")}
                                />
                            </View>
                            <Text style={registerStyles.error}>{formikProps.touched.name && formikProps.errors.name}</Text>
                            <View style={registerStyles.text}>
                                <TextInput style={registerStyles.textContent}
                                    placeholder="Email"
                                    onChangeText={formikProps.handleChange("email")}
                                    value={formikProps.values.email}
                                    onBlur={formikProps.handleBlur("email")}
                                />
                            </View>
                            <Text style={registerStyles.error}>{formikProps.touched.email && formikProps.errors.email}</Text>
                            <View style={registerStyles.text}>
                                <TextInput style={registerStyles.textContent}
                                    placeholder="Password"
                                    secureTextEntry={secure}
                                    onChangeText={formikProps.handleChange("password")}
                                    value={formikProps.values.password}
                                    onBlur={formikProps.handleBlur("password")}
                                />
                                <TouchableOpacity onPress={() => changeImage()}>
                                    <FontAwesome5 name={icon} size={20} color="black" style={registerStyles.icon} />
                                    {/* <Image resizeMode="contain" style={registerStyles.icon} source={ === 'eye-slash' ? require('../../../image/icons/eye-slash.png') : require('../../../image/icons/eye.png')} /> */}
                                </TouchableOpacity>
                            </View>
                            <Text style={registerStyles.error}>{formikProps.touched.password && formikProps.errors.password}</Text>
                            <View style={registerStyles.text}>
                                <TextInput style={registerStyles.textContent}
                                    placeholder="Confirm password"
                                    secureTextEntry={secureConfirm}
                                    onChangeText={formikProps.handleChange("confirmPassword")}
                                    value={formikProps.values.confirmPassword}
                                    onBlur={formikProps.handleBlur("confirmPassword")}
                                />
                                <TouchableOpacity onPress={() => changeImageConfirm()}>
                                    <FontAwesome5 name={iconConfirm} size={20} color="black" style={registerStyles.icon} />
                                    {/* <Image resizeMode="contain" style={registerStyles.icon} source={ === 'eye-slash' ? require('../../../image/icons/eye-slash.png') : require('../../../image/icons/eye.png')} /> */}
                                </TouchableOpacity>
                            </View>
                            <Text style={registerStyles.error}>{formikProps.touched.confirmPassword && formikProps.errors.confirmPassword}</Text>

                            <FlatButton
                                title={'Sign up'}
                                onPress={() => formikProps.handleSubmit()} />
                            <Text style={{ ...registerStyles.error, textAlign: "center", top: 0, marginTop: 5, marginLeft: 0 }}>{error}</Text>
                            <ActivityIndicator size="large" color="blue" animating={loading} />
                        </View>
                    )}
                </Formik>
            </ScrollView>
        </View>
    )
}
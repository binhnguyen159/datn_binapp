import React, { useEffect } from 'react'
import { Button, Text, View } from 'react-native'
import auth from '@react-native-firebase/auth';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import Add from './main/addScreen/add'
import Feed from './main/feedScreen/feed'
import Search from './main/searchScreen/search'
import Profile from './main/profileScreen/profile'
import drawerContent from './main/profileScreen/drawer/drawerContent';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { useDispatch } from 'react-redux';
import { fetchUser, fetchUserPosts, fetchUserFollowing, clearData, fetchImagePostsUser, fetchVideoPostsUser } from '../redux/actions/user.actions';
import { clearDatas } from '../redux/actions/users.actions';
import save from './main/saveScreen/save';
import { fetchMessages } from '../redux/actions/messages.action';


const Tab = createBottomTabNavigator();

export default function main() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(clearData())
        dispatch(clearDatas())
        dispatch(fetchUser());
        dispatch(fetchUserPosts());
        dispatch(fetchUserFollowing())
        dispatch(fetchMessages());
    }, [])


    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    switch (route.name) {
                        case 'Feed': iconName = 'home'; break;
                        case 'Add': iconName = 'plus-circle'; break;
                        case 'Search': iconName = 'search'; break;
                        case 'Profiles': iconName = 'user-alt'; break;
                    }
                    return <FontAwesome5 name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: 'blue',
                inactiveTintColor: 'gray',
                keyboardHidesTabBar: true,
            }}
        >
            <Tab.Screen name='Feed' component={Feed} />
            <Tab.Screen name='Add' component={save} />
            <Tab.Screen name='Search' component={Search} />
            <Tab.Screen name='Profiles' component={drawerContent}
                listeners={{
                    tabPress: e => {
                        dispatch(fetchUserPosts());
                        dispatch(fetchImagePostsUser(auth().currentUser.uid));
                        dispatch(fetchVideoPostsUser(auth().currentUser.uid));
                        // e.preventDefault();
                    }
                }}
            />
        </Tab.Navigator>
    )
}

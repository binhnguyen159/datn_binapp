import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

export default function FlatButton2nd({ title, onPress }) {
    return (
        <View style={styles.button}>
            <TouchableOpacity onPress={onPress}>
                <Text style={styles.text}>{title}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    button: {
        flexDirection: 'column',
        marginHorizontal: 20,
        marginBottom: 15,
    },
    text: {
        fontWeight: 'bold',
        backgroundColor: 'white',
        fontSize: 16,
        textAlign: 'center',
        color: 'black',
        paddingVertical: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'gray',
    }
})
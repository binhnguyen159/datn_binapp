import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

export default function FlatButton({ title, onPress }) {
    return (
        <View style={styles.button}>
            <TouchableOpacity onPress={onPress}>
                <Text style={styles.text}>{title}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    button: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        marginHorizontal: '20%',
        marginTop: 15,
        borderWidth: 2,
        borderColor: 'gray',
        borderRadius: 15,
    },
    text: {
        fontWeight: 'bold',
        backgroundColor: 'white',
        fontSize: 16,
        textAlign: 'center',
        color: 'black',
        paddingVertical: 15,
        borderRadius: 15,
    }
})
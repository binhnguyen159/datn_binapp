import React from 'react'
import { TouchableOpacity } from 'react-native'
import { View, Text, StyleSheet } from 'react-native'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function header({ title, onPress, navigation, icon }) {
    return (
        <View style={styles.container}>
            <View style={styles.headerLeft}>
                <TouchableOpacity onPress={
                    () => {
                        navigation.goBack();
                    }
                } >
                    <Ionicons name="arrow-back" size={30} color="black" />
                </TouchableOpacity>
                <Text style={styles.title}>{title}</Text>
            </View>
            <TouchableOpacity onPress={onPress}>
                <FontAwesome5Icon name={icon} size={24} color='black' />
            </TouchableOpacity>
        </View >
    )
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 15

    },
    title: {
        fontWeight: '500',
        fontSize: 24,
        marginLeft: 20
    },
    headerLeft: {
        flexDirection: 'row',
    }
})

import firebase from '@react-native-firebase/app'
import firestore from '@react-native-firebase/firestore'
import auth from '@react-native-firebase/auth';

import { fetchUsersData } from './users.actions';
import {
    USER_STATE_CHANGE,
    USER_POSTS_STATE_CHANGE,
    USER_FOLLOWING_STATE_CHANGE,
    CLEAR_DATA,
    USER_SEARCH_STATE_CHANGE
} from '../states/user.states'


export function clearData() {
    return (dispatch => {
        dispatch({ type: CLEAR_DATA })
    })
}

export function fetchUser() {
    console.log('fetchUser');
    return ((dispatch) => {
        firestore()
            .collection('users')
            .doc(auth().currentUser.uid)
            .get()
            .then((snapshot) => {
                if (snapshot.exists) {
                    dispatch({ type: USER_STATE_CHANGE, currentUser: snapshot.data() })
                }
                else {
                    console.log('does not exist');
                }
            })
    })
}


export function fetchUserFollowing() {
    console.log('fetchUserFollowing');
    return (async (dispatch) => {
        var unsubscribe = await firestore()
            .collection('following')
            .doc(auth().currentUser.uid)
            .collection('userFollowing')
            .onSnapshot(snapshot => {
                let following = snapshot.docs.map(doc => {
                    const id = doc.id;
                    return id
                })
                following.push(auth().currentUser.uid);
                dispatch({ type: USER_FOLLOWING_STATE_CHANGE, following });
                for (let index = 0; index < following.length; index++) {
                    dispatch(fetchUsersData(following[index]));

                }
            })

        // unsubscribe()
    })
}


export function fetchUserPosts() {
    console.log('fetchUserPosts');
    return ((dispatch) => {
        firestore()
            .collection('posts')
            .doc(auth().currentUser.uid)
            .collection('userPosts')
            .orderBy('creation', 'asc')
            .get()
            .then(snapshot => {
                let posts = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data }
                });
                dispatch({ type: USER_POSTS_STATE_CHANGE, posts })
            })
            .catch(error => console.log(error));
    })
}
export function fetchFindPostsUser(uid) {
    return (dispatch => {
        firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .orderBy('creation', 'desc')
            .get()
            .then(snapshot => {
                let posts = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data, uid }
                });
                dispatch({ type: USER_POSTS_STATE_CHANGE, posts })
            })
            .catch(error => console.log(error));
    })
}

export function fetchImagePostsUser(uid) {
    return (dispatch => {
        firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .orderBy('creation', 'desc')
            .onSnapshot(snapshot => {
                let posts = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data, uid }
                });
                const data1 = posts.filter(post => post.type !== 'video')
                dispatch({ type: 'USER_POSTS_IMAGE_STATE_CHANGE', posts: data1 })
            })
        // .catch(error => console.log(error));
    })
}

export function fetchVideoPostsUser(uid) {
    return (dispatch => {
        firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .orderBy('creation', 'desc')
            .onSnapshot(snapshot => {
                let posts = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data, uid }
                });
                const data = posts.filter(post => post.type === 'video')
                dispatch({ type: 'USER_POSTS_VIDEO_STATE_CHANGE', posts: data })
            })
        // .catch(error => console.log(error));
    })
}


export function fetchSearchUsers(value) {
    return (dispatch => {
        firestore()
            .collection('users')
            .where('name', '>=', value)
            .get()
            .then(snapshot => {
                let usersSearch = snapshot.docs.map(user => {
                    const data = user.data();
                    const id = user.id;
                    return { id, ...data }
                })
                dispatch({ type: USER_SEARCH_STATE_CHANGE, usersSearch })
            })
    })
}

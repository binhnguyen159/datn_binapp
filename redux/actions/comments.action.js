import { findUserById } from './../../utils/function';
import firestore from '@react-native-firebase/firestore';

export function fetchComments(uid, postId) {
    // console.log('fetchComments');
    return (dispatch) => {
        firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postId)
            .collection('comments')
            .orderBy('creation', 'desc')
            .onSnapshot(snapshot => {
                const comments = snapshot.docs.map((item) => {
                    const data = item.data();
                    const id = item.id;
                    return { id, ...data }
                });
                dispatch({ type: 'LOAD_COMMENTS', comments })
                for (let index = 0; index < comments.length; index++) {
                    findUserById(comments[index].creator)
                        .then(user => {
                            const value = { id: comments[index].id, user }
                            dispatch({ type: 'LOAD_INFO_USER', comment: value })
                        })
                }
            });
    }
}

export function clearListComments() {
    return (dispatch => {
        dispatch({ type: 'CLEAR_LIST_COMMENTS' })
    })
}
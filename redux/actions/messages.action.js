
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { getLastMess } from './../../utils/function';
import {
    MESSAGES_STATE_CHANGE,
    MESSAGE_STATE_CHANGE
} from '../states/user.states';

export function fetchUnRead(roomId) {
    return (dispatch) => {
        dispatch({ type: "UNREAD_MESS", roomId })
    }
}

export function fetchMessages() {
    return (async dispatch => {
        let messages = [];
        await firestore()
            .collection('messages')
            .where('members', 'array-contains', auth().currentUser.uid)
            .onSnapshot(snapshot => {
                snapshot.docs.map(doc => {
                    const id = doc.id;
                    const data = doc.data();
                    let uid = data.members[0];
                    if (data.members[0] === auth().currentUser.uid) {
                        uid = data.members[1];
                    }
                    firestore()
                        .collection('users')
                        .doc(uid)
                        .get()
                        .then(async snapshot => {
                            if (snapshot.exists) {
                                const roomId = id;
                                const uid = snapshot.id;
                                const user = snapshot.data();
                                const lastMess = await getLastMess(roomId);
                                const message = { roomId, uid, user, lastMess }
                                messages.push(message);
                                // dispatch({ type: MESSAGES_STATE_CHANGE, message })
                                return messages;
                            }
                        })
                        .then(value => {
                            if (value.length === snapshot.docs.length) {
                                dispatch({ type: MESSAGES_STATE_CHANGE, messages: value })
                                messages = []
                            }
                        })

                })

            })
    })
}

export function fetchListContentMessage(roomId, page) {
    return (dispatch => {
        firestore()
            .collection('messages')
            .doc(roomId)
            .collection('contents')
            .orderBy('creation', 'desc')
            // .limit(page * 15)
            .onSnapshot(snapshot => {
                let data = snapshot.docs.map(item => {
                    const id = item.id;
                    const data = item.data();
                    return { id, ...data }
                })
                dispatch({ type: MESSAGE_STATE_CHANGE, message: data })
            })
    })
}
export function fetchReceiver(uid) {
    return (dispatch => {
        firestore()
            .collection('users')
            .doc(uid)
            .onSnapshot(snapshot => {
                if (snapshot.exists) {
                    const id = snapshot.id;
                    const data = snapshot.data();
                    // console.log('fetchReceiver', snapshot.data().email);
                    dispatch({ type: "RECEIVER", receiver: { id, data } })
                }
            })
    })

}

export function clearDataMess() {
    return (dispatch => {
        dispatch({ type: 'CLEAR_DATA_MESS' })
    })
}


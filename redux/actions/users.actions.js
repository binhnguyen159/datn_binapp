import firestore from '@react-native-firebase/firestore'
import auth from '@react-native-firebase/auth';
import {
    USERS_STATE_CHANGE,
    CLEAR_DATA,
    USERS_POSTS_STATE_CHANGE,
    USERS_LIKES_STATE_CHANGE
} from '../states/user.states'

export function clearDatas() {
    return (dispatch => {
        dispatch({ type: CLEAR_DATA })
    })
}
export function clearFeed(uid) {
    return (dispatch => {
        dispatch({ type: 'CLEAR_FEED', uid })
    })
}


export function fetchUsersData(uid,) {
    return ((dispatch, getState) => {
        const found = getState().usersState.users.some(el => el.uid === uid);
        if (!found) {
            firestore()
                .collection('users')
                .doc(uid)
                .get()
                .then((snapshot) => {
                    if (snapshot.exists) {
                        let user = snapshot.data();
                        user.uid = snapshot.id;
                        dispatch({ type: USERS_STATE_CHANGE, user });
                        dispatch(fetchUsersFollowingPosts(user.uid))
                    }
                    else {
                        console.log('does not exist');
                    }
                })
                .catch(error => console.log(error));
        }
    })
}

export function fetchUsersFollowingPosts(uid) {
    return ((dispatch, getState) => {
        firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .orderBy('creation', 'asc')
            .get()
            .then(snapshot => {
                const user = getState().usersState.users.find(el => el.uid === uid);
                let posts = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data, user }
                });
                for (let index = 0; index < posts.length; index++) {
                    dispatch(fetchUsersFollowingLikes(uid, posts[index].id))
                }
                dispatch({ type: USERS_POSTS_STATE_CHANGE, posts, uid })
            })
            .catch(error => console.log(error));
    })
}

export function fetchUserAddPosts(dataAdd) {
    return ((dispatch) => {
        dispatch({ type: 'USER_ADD_POST_CHANGE', posts: dataAdd })
        dispatch(fetchUsersFollowingLikes(dataAdd.user.uid, dataAdd.id))
    })
}

export function fetchUsersFollowingLikes(uid, postId) {
    return ((dispatch) => {
        firestore()
            .collection('posts')
            .doc(uid)
            .collection('userPosts')
            .doc(postId)
            .collection('likes')
            .doc(auth().currentUser.uid)
            .onSnapshot(snapshot => {
                // const postId = snapshot.ZE.path.segments[3];

                let currentUserLike = false;
                if (snapshot.exists) {
                    currentUserLike = true;
                }
                dispatch({ type: USERS_LIKES_STATE_CHANGE, postId, currentUserLike })
            })
    })
}

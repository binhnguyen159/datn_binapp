import { combineReducers } from 'redux'
import { comments } from './comments.reducer';
import { messages } from './messages.reducer';
import { user } from './user.reducers'
import { users } from './users.reducers';

const Reducers = combineReducers({
    userState: user,
    usersState: users,
    messState: messages,
    commentsState: comments
})

export default Reducers;
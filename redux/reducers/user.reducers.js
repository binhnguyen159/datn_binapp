// import * as state from "../states/user.states";
import {
    USER_STATE_CHANGE,
    USER_POSTS_STATE_CHANGE,
    USER_FOLLOWING_STATE_CHANGE,
    CLEAR_DATA,
    USER_SEARCH_STATE_CHANGE
} from '../states/user.states';

const initialState = {
    currentUser: null,
    posts: [],
    following: [],
    follower: [],
    usersSearch: [],
    imagePosts: [],
    videoPosts: [],
};



export const user = (state = initialState, action) => {
    switch (action.type) {
        case USER_STATE_CHANGE:
            return {
                ...state,
                currentUser: action.currentUser,
            }
        case USER_POSTS_STATE_CHANGE:
            return {
                ...state,
                posts: action.posts,
            }
        case USER_FOLLOWING_STATE_CHANGE:
            return {
                ...state,
                following: action.following,
            }
        case USER_SEARCH_STATE_CHANGE:
            return {
                ...state,
                usersSearch: action.usersSearch,
            }
        case 'USER_POSTS_IMAGE_STATE_CHANGE':
            return {
                ...state,
                imagePosts: action.posts,
            }
        case 'USER_POSTS_VIDEO_STATE_CHANGE':
            return {
                ...state,
                videoPosts: action.posts,
            }
        case CLEAR_DATA:
            return initialState
        default:
            return state;
    }
}
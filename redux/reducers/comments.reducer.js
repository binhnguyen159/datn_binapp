const initialState = {
    comments: [],
}

export const comments = (state = initialState, action) => {
    switch (action.type) {
        case 'LOAD_COMMENTS': {
            return {
                comments: action.comments,
            }
        }
        case 'LOAD_INFO_USER': {
            return {
                comments: state.comments.map(item => item.id == action.comment.id ?
                    {
                        ...item,
                        user: action.comment.user
                    }
                    :
                    item
                )
            }
        }
        case 'CLEAR_LIST_COMMENTS': {
            return initialState
        }
        default:
            return state
    }
}
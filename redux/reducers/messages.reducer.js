// import * as state from "../states/user.states";
import {
    MESSAGES_STATE_CHANGE,
    MESSAGE_STATE_CHANGE
} from '../states/user.states';

const initialState = {
    receiver: null,
    listMessageOfOne: [],
    messages: []
};



export const messages = (state = initialState, action) => {
    switch (action.type) {
        case MESSAGES_STATE_CHANGE:
            return {
                ...state,
                messages: action.messages//[...state.messages, action.messages]
            }
        case MESSAGE_STATE_CHANGE:
            return {
                ...state,
                listMessageOfOne: action.message
            }
        case "RECEIVER":
            return {
                ...state,
                receiver: action.receiver
            }
        case "UNREAD_MESS":
            return {
                ...state,
                messages: state.messages.map(mess => mess.roomId === action.roomId ?
                    { ...mess, hasRead: true }
                    :
                    mess
                )
            }
        case "CLEAR_DATA_MESS":
            return initialState
        default:
            return state;
    }
}